#!/usr/bin/env python3
from ..common import c_io
import logging
import shutil
import numpy as np
from .qprop import *
iteration_number = 0


class simulation_result_container():
    def __init__(self, l_grid_size):
        self.ionization = 0
        self.l_grid_size = l_grid_size


def write_iteration_output(iter_parameters):
    stringtemplate = """[Output]
iteration_number = %(iteration_number)s
overlap = %(overlap)s
ionization = %(ionization)s
laser-max-amplitude = %(laser-max-amplitude)s
laser-fluence = %(laser-fluence)s
laser-duration = %(laser-duration)s
laser-integral = %(laser-integral)s
laser-duration-fwhm = %(duration-fwhm)s
laser-duration-intensity-fwhm = %(duration-intensity-fwhm)s
    """
    filecontents = stringtemplate % iter_parameters
    f = open('opt_out/iter_out_' + str(iteration_number).zfill(6), 'w')
    f.write(filecontents)
    f.close()

# A target function for optimization of the photoelectron spectrum. Is compatible with nlopt.
# Input:
#   x           the laser parameters, this is changed by the optimization routine
#   grad        gradient to be calculated by the routine (not implemented)
#   laser       instance of the laser class
#   result_container
#               a container where we save some simulation info each iteration
#               to be passed on to the constraint evaluators
#   sconfig     simulation configuration parameters
#   staterange  Range of the n-quantum numbers of the Rydberg states we wish to occupy
# Output:
#   overlap with the selected Rydberg states


def target_rydberg(x, grad, laser, result_container, sconfig, states,
                   rydberg_states):
    global iteration_number

    logger = logging.getLogger('')
    calculate_gradient = True if grad.size > 0 else False

    laser.set_vars(x)
    laser.print_parameters()

    T0 = laser.beginning_of_pulse()
    Tmax = laser.end_of_pulse()
    logger.info("Simulation time: %.1f - %.1f" % (T0, Tmax))

    # Make time to start at t=0
    TIME = np.linspace(T0, Tmax, 10000) - T0

    def Elaser(t): return laser.electric_field(T0 + t)

    c_io.savetxt('opt_out/laser_' + str(iteration_number).zfill(6),
                 np.vstack((TIME - TIME[0], Elaser(TIME))).T, header=laser.get_parameters_str())

    # Calculate vector potential
    vecpot = -laser(T0 + TIME)

    c_io.savetxt('opt_out/vecpot_' + str(iteration_number).zfill(6),
                 np.vstack((TIME - TIME[0], vecpot)).T, header=laser.get_parameters_str())
    c_io.savetxt('simulation/vecpot',
                 np.vstack((TIME - TIME[0], vecpot)).T, header=None)

    # Run
    sconfig['vecpot-file'] = 'vecpot' 

    if calculate_gradient:
        sconfig['calculate-costate'] = "1"
    else:
        sconfig['calculate-costate'] = "0"
    qprop_write_input_file(sconfig)
    qprop_calculate_td()

    # Calculate ionization
    ionization = qprop_get_ionization()
    _, overlap = rydberg_states.calculate_overlaps(sconfig,
                                                               states)

    result_container.ionization = ionization
    logger.info("   Ionization: %.2f" % ionization)
    logger.info("   Overlap with Rydberg states: %.2f" % overlap)
    logger.info("")

    # Iteration output
    iter_output = {}
    iter_output['iteration_number'] = iteration_number
    iter_output['overlap'] = overlap
    iter_output['ionization'] = ionization
    iter_output['laser-max-amplitude'] = laser.max_field()
    iter_output['laser-fluence'] = laser.calculate_fluence()
    iter_output['laser-duration'] = laser.duration()
    iter_output['laser-integral'] = laser.integral()
    iter_output['duration-fwhm'] = laser.duration_fwhm()
    iter_output['duration-intensity-fwhm'] = laser.duration_intensity_fwhm()
    write_iteration_output(iter_output)

    # Copy results
    shutil.copyfile('simulation/real_time-wf.dat',
                    'opt_out/real_time-wf.dat_' + str(iteration_number).zfill(6))
    shutil.copyfile('simulation/real_time-yield.dat',
                    'opt_out/real_time-yield.dat_' + str(iteration_number).zfill(6))
    try:
        shutil.copyfile('simulation/tsurff-partial0.dat',
                        'opt_out/tsurff-partial0.dat_' + str(iteration_number).zfill(6))
        shutil.copyfile('simulation/tsurff-polar0.dat',
                        'opt_out/tsurff-polar0.dat_' + str(iteration_number).zfill(6))
    except:
        pass

    iteration_number = iteration_number + 1

    return overlap
