#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pes_target.py
#
#  Copyright 2016 Janne Solanpää <janne@solanpaa.fi>
#
from scipy.integrate import cumtrapz, simps
from ..common import c_io
import logging
import shutil
import numpy as np
from .qprop import *

# Set the iteration number to zero in the beginning
iteration_number = 0

# A container for level of ionization and the grid size of the l-quantum number


class simulation_result_container():
    def __init__(self, l_grid_size):
        self.ionization = 0
        self.l_grid_size = l_grid_size


# A target function for optimization of the photoelectron spectrum. Is compatible with nlopt.
# Input:
#   x           the laser parameters, this is changed by the optimization routine
#   grad        gradient to be calculated by the routine (not implemented)
#   laser       instance of the laser class
#   result_container
#               a container where we save some simulation info each iteration
#               to be passed on to the constraint evaluators
#   sconfig     simulation configuration parameters
#   Ea          beginning of the PES target range
#   Eb          end of the PES target range
def target_pes(x, grad, laser, result_container, sconfig, Ea, Eb):
    global iteration_number

    logger = logging.getLogger('')
    calculate_gradient = True if grad.size > 0 else False
    assert(not calculate_gradient)
    laser.set_vars(x)
    laser.print_parameters()

    T0 = laser.beginning_of_pulse()
    Tmax = laser.end_of_pulse()
    logger.info("Simulation time: %.1f - %.1f" % (T0, Tmax))

    # Make time to start at t=0
    TIME = np.linspace(T0, Tmax, 10000) - T0

    def Elaser(t): return laser(T0 + t)

    c_io.savetxt('opt_out/laser_' + str(iteration_number).zfill(6),
                 np.vstack((TIME, Elaser(TIME))).T, header=laser.get_parameters_str())

    # Calculate vector potential
    vecpot = -cumtrapz(Elaser(TIME), TIME)

    c_io.savetxt('opt_out/vecpot_' + str(iteration_number).zfill(6),
                 np.vstack((TIME[1:] - TIME[1], vecpot)).T, header=laser.get_parameters_str())
    c_io.savetxt('simulation/vecpot',
                 np.vstack((TIME[1:] - TIME[1], vecpot)).T, header=None)

    # Run
    sconfig['vecpot-file'] = 'vecpot'
    converged = False
    while(not converged):
        qprop_write_input_file(sconfig)
        qprop_calculate_td()
        # Check convergence wrt l-quantum number
        if(sconfig['check-ell-convergence']):
            qprop_calculate_total_pes(sconfig)
            converged = qprop_check_ell_convergence(sconfig)
            if(not converged):
                logging.info('  Calculation not converged wrt. ell-grid-size.')
                new_ell_grid_size = min(
                    sconfig['ell-grid-max-size'], sconfig['ell-grid-size'] + max(2, int(sconfig['ell-grid-size'] * 0.2)))
                if(new_ell_grid_size == sconfig['ell-grid-max-size']):
                    converged = True
                    logging.info('  Reached maximum ell-grid-size.')
                    sconfig['ell-grid-size'] = new_ell_grid_size
                    qprop_write_input_file(sconfig)
                    qprop_calculate_gs()
                    qprop_calculate_td()
                else:
                    logging.info('  Increasing ell-grid-size to %d' %
                                 new_ell_grid_size)
                    sconfig['ell-grid-size'] = new_ell_grid_size
                    result_container.l_grid_size = new_ell_grid_size
                qprop_write_input_file(sconfig)
                qprop_calculate_gs()
        else:
            converged = True

    # Calculate ionization
    ionization = qprop_get_ionization()
    result_container.ionization = ionization
    logger.info("   Ionization: %.2f" % ionization)

    # Copy results
    shutil.copyfile('simulation/hydrogen_re-wf.dat',
                    'opt_out/hydrogen_re-wf.dat_' + str(iteration_number).zfill(6))
    shutil.copyfile('simulation/hydrogen_re-yield.dat',
                    'opt_out/hydrogen_re-yield.dat_' + str(iteration_number).zfill(6))
    shutil.copyfile('simulation/tsurff-partial0.dat',
                    'opt_out/tsurff-partial0.dat_' + str(iteration_number).zfill(6))
    shutil.copyfile('simulation/tsurff-polar0.dat',
                    'opt_out/tsurff-polar0.dat_' + str(iteration_number).zfill(6))

    # Calculate integral of total PES
    idx1 = np.where(energy > Ea)[0][0]
    idx2 = np.where(energy < Eb)[0][-1]
    target_value = simps(PES[idx1:idx2], energy)

    return target_value
