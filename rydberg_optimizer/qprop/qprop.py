#!/usr/bin/env python3

import os
import sys
import numpy as np

from .core import real_time_propagation, eval_tsurff

# Writes the input file for qprop to the folder 'simulation'
# Input:
#    sconfig    a dictionary of the input parameters
def qprop_write_input_file(sconfig):
    stringtemplate = """delta-r double %(delta-r)s
radial-grid-size double %(radial-grid-length)s
ell-grid-size long %(ell-grid-size)s
qprop-dim long %(qprop-dim)s
initial-m long %(initial-m)s
initial-ell long %(initial-ell)s

imag-width double %(imag-width)s
# time step for propagation; delta-r/4 is a sensible choice
delta-t double %(delta-t)s
#vector potential file
vecpot-file string %(vecpot-file)s

# distance to the t-SURFF boundary
use_tsurff bool %(check-ell-convergence)s
R-tsurff double %(r-tsurff)s
# This determines the duration of the simulation (slowest electron to reach the t-SURFF boundary).
p-min-tsurff double %(p-min-tsurff)s
# largest k value in the calculated spectrum
k-max-surff double %(k-max-tsurff)s
# number of k values for the spectrum
num-k-surff long %(num-k-tsurff)s
# number of angles theta (\theta \in [0:\pi])
num-theta-surff long %(num-theta-tsurff)s
# number of angles phi (\phi \in [0:2\pi])
num-phi-surff long %(num-phi-tsurff)s
# delta-k-scheme=1: equidistant k grid discretization w.r.t momentum; delta-k-scheme=2: equidistant k grid discretization w.r.t energy
delta-k-scheme long %(delta-k-scheme)s
# how many time steps are processed at a time during evaluation of the spectrum
cache-size-t long %(cache-size-t)s
# expansion-scheme=2: true expansion of the spectrum in spherical harmonics
# expansion-scheme=1: use for small number of angles and large number of ells (no partial spectra are produced)
expansion-scheme long %(expansion-scheme)s

save-interval double %(save-interval)s

calculate-costate bool %(calculate-costate)s

atom_type string %(atom-type)s
"""
    filecontents = stringtemplate % sconfig
    # Write the qprop input file
    f = open('simulation/simul.param', 'w')
    f.write(filecontents)
    f.close()


def qprop_prepare_initial_state(sconfig, rydberg_states):
    n,l = sconfig['initial-state']
    assert l < n, "Initial state is not valid: l >= n"
    assert l >= 0, "Initial state is not valid: l<0"
    assert n > 0, "Initial state is not valid: n<=0"
    wf = rydberg_states.get_state(sconfig, n,l)
    ell_npts = int(sconfig['ell-grid-size'])

    r_npts = len(wf)
    wf_to_file = np.zeros((ell_npts*r_npts, 2))
    wf_to_file[l*r_npts:(l+1)*r_npts,0] = wf

    np.savetxt('simulation/initial_state.dat', wf_to_file)


# Calculates the time evolution


def qprop_calculate_td():
    os.chdir('simulation')
    real_time_propagation()
    os.chdir('../')

# Calculates the total angle-integrated photoelectron spectrum for m=0
# Input:
#     sconfig     dictionary of the simulation parameters
# Output:
#     energy
#     photoelectron yield


def qprop_calculate_total_pes(sconfig):
    os.chdir('simulation')
    eval_tsurff()
    os.chdir('../')
    # Total PES for m=0 state
    pesdata = np.loadtxt('simulation/tsurff-partial0.dat')
    return pesdata[:, 0], pesdata[:, -1]

# Checks convergence with respect to the l-quantum number
# Input:
#    sconfig    dictionary of the simulation parameters
# Output:
#    true/false true if converged, false if not


def qprop_check_ell_convergence(sconfig):
    data = np.loadtxt('simulation/tsurff-partial0.dat')
    tmp = data[:, -2] / data[:, -1]
    idx = np.isnan(tmp)
    value = np.mean(tmp[~idx])
    return value < sconfig['ell-convergence-threshold']

# Returns overlap of the final state with the initial state


def qprop_get_initial_state_overlap():
    data = np.loadtxt('simulation/real_time-yield.dat')
    return data[1]

def qprop_get_ionization():  # 1 = fully ionized; 0 = no emission at all
    data = np.loadtxt('simulation/real_time-yield.dat')
    return data[0]
