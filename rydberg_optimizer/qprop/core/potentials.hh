#ifndef _POTENTIALS_HH_
#define _POTENTIALS_HH_
// definitions of potentials

// MODEL POTENTIALS FOR ALKALI METAL ATOMS AND Li-LIKE IONS, W. Schweizer, P. Fassbinder, R. Gonzalez-Ferez
class lithium_sae_atom {
private:
    static constexpr double a1 = 3.395;
    static constexpr double a2 = 3.212;
    static constexpr double a3 = 3.207;
    static constexpr double Z  = 3.0;
    static constexpr double Zt = 1.0;
public:
  double operator()(double r, double l, double m, double time, int me) const {
    return -1.0/r*(Zt+(Z-Zt)*exp(-a1*r)+a2*r*exp(-a3*r));
  };
};

class hydrogen_atom {
public:
  double operator()(double r, double l, double m, double time, int me) const {
    return -1.0/r;
  };
};


class always_zero2 {
public:
    double operator()(double t, int me) const {
        return 0.0;
    }
    double get_duration() {
        return 0.0;
    }

    double integral(double time) {
        return 0.0;
    }
};

class always_zero5 {
public:
    double operator()(double, double, double, double t, int me) const {
        return 0.0;
    }
    double get_duration() {
        return 0.0;
    }

    double integral(double time) {
        return 0.0;
    }
};

class imagpot {
  long imag_potential_width;
  double ampl_im;  // amplitude of imaginary absorbing potential  <--------------  100.0 imag pot on,  0.0 off
public:
  imagpot(long width, double ampl=100.0) : ampl_im(ampl), imag_potential_width(width) {
    //
  };
  double operator()(long xindex, long yindex, long zindex, double time, grid g) {
    if (ampl_im>1.0) {
      const long imag_start=g.ngps_x()-imag_potential_width;
      if (xindex<imag_start)
    return 0;
      else {
    const double r=double(xindex-imag_start)/double(imag_potential_width);
    return ampl_im*pow2(pow2(pow2(pow2(r))));
      };
    }
    else {
      return 0.0;
    };
  };
};

class noimagpot {
public:
  noimagpot() = default;
  double operator()(long xindex, long yindex, long zindex, double time, grid g) {
      return 0.0;
  };
};

#endif // _POTENTIALS_HH_
