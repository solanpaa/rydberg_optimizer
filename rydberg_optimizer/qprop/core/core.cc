#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/args.hpp>
#include "qprop.hh"

using namespace boost::python;

// Expose to python
BOOST_PYTHON_MODULE(core) {

    def("real_time_propagation", real_time_propagation, "real_time_propagation");

    def("eval_tsurff", eval_tsurff, "eval_tsurff");

}
