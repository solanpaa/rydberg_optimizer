#include <memory>
#include <complex>
#include <string>
#include <iostream>

typedef std::complex<double> cplxd;
typedef std::unique_ptr<cplxd[]> cplxd_ptr;

#include <tsurffSpectrum.hh>
#include <potentials.hh>
#include "interp_function.hh"
using std::string;

void eval_tsurff() {
    parameterListe params("simul.param");

    // only A_z in this example
    interp_function vecpot_z_fun{"vecpot"};
    function_wrapper_1d<interp_function, 10000, 1> vecpot_z{vecpot_z_fun};


    tsurffSpectrum<always_zero2, always_zero2, function_wrapper_1d<interp_function, 10000, 1>> tsurff(params, always_zero2{}, always_zero2{}, vecpot_z);
    tsurff.time_integration();
    // tsurff.print_int_dt_psi();
    tsurff.polar_spectrum();
    tsurff.print_partial_amplitudes();
    //  tsurff.print_Plms();
}
