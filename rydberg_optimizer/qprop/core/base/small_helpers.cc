#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>
#include "smallHelpers.hh"


FILE* fopen_with_check(std::string fname, const char* attr) {
  FILE* file_ptr = fopen(fname.c_str(), attr);
  if (file_ptr==NULL) {
    std::cerr << fname << " can not be opened!" << std::endl;
    exit(-1);
  };
  return file_ptr;
};


