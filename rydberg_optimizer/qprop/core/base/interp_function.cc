#include "interp_function.hh"

/*
 * Reads vectorpotential from two-column ASCII file where the first column is the argument value and second the function value
 * Input:
 *     filename
 * Output:
 *     argument vector
 *     function value vector
 */
std::pair<std::vector<double>, std::vector<double>> read_function_from_file(std::string filename){
    std::fstream fs{filename.c_str(), std::ios::in};
    double a, b;
    std::vector<double> x,fval;
    for(std::string line; std::getline(fs, line);){
        if((line.at(0)!='#') and (line.at(0) != '%')){

            std::stringstream ss{line};
            if(ss >> a >> b){
                x.push_back(a);
                fval.push_back(b);
            } else{
                throw std::domain_error("Invalid line when reading a user supplied function.");
            }
        }

    }
    return std::make_pair(x,fval);

}


