#ifndef _INTERPOLATED_FUNCTION_HH_
#define _INTERPOLATED_FUNCTION_HH_
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <string>
#include <fstream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <gsl/gsl_integration.h>
#include <iostream>
#include <memory>
#include <functional>
#include <type_traits>
#include <vector>
std::pair<std::vector<double>, std::vector<double>> read_function_from_file(std::string filename);

/*
 * Interpolates a 1D function with values read from an ASCII file.
 */
class interp_function {
public:
    /*
     * Input:
     *    filename    path to the ascii file
     */
    interp_function(std::string filename) :
        _x_f{read_function_from_file(filename)},
        _acc{gsl_interp_accel_alloc(),&gsl_interp_accel_free},
        _spline{gsl_spline_alloc(gsl_interp_akima, std::get<0>(_x_f).size()), &gsl_spline_free} {

        //_acc = std::unique_ptr<gsl_interp_accel,decltype(gsl_interp_accel_free)>(gsl_interp_accel_alloc(),&gsl_interp_accel_free);
        _xmin = *std::min_element(std::get<0>(_x_f).begin(), std::get<0>(_x_f).end());
        _xmax = *std::max_element(std::get<0>(_x_f).begin(), std::get<0>(_x_f).end());

        gsl_spline_init(_spline.get(), std::get<0>(_x_f).data(), std::get<1>(_x_f).data(), std::get<0>(_x_f).size());
    }

    interp_function(std::vector<double>& x, std::vector<double>& y) :
        _x_f{x,y},
        _acc{gsl_interp_accel_alloc(),&gsl_interp_accel_free},
        _spline{gsl_spline_alloc(gsl_interp_akima, std::get<0>(_x_f).size()), &gsl_spline_free} {

        //_acc = std::unique_ptr<gsl_interp_accel,decltype(gsl_interp_accel_free)>(gsl_interp_accel_alloc(),&gsl_interp_accel_free);
        _xmin = *std::min_element(std::get<0>(_x_f).begin(), std::get<0>(_x_f).end());
        _xmax = *std::max_element(std::get<0>(_x_f).begin(), std::get<0>(_x_f).end());

        gsl_spline_init(_spline.get(), std::get<0>(_x_f).data(), std::get<1>(_x_f).data(), std::get<0>(_x_f).size());
    }

    interp_function(const interp_function& c) :
        _x_f{c._x_f},
        _acc{gsl_interp_accel_alloc(),&gsl_interp_accel_free},
        _spline{gsl_spline_alloc(gsl_interp_akima, std::get<0>(_x_f).size()), &gsl_spline_free},
        _xmin{c._xmin},
        _xmax{c._xmax} {
            gsl_spline_init(_spline.get(), std::get<0>(_x_f).data(), std::get<1>(_x_f).data(), std::get<0>(_x_f).size());
        }
    interp_function(interp_function&& c) :
        _acc{gsl_interp_accel_alloc(),&gsl_interp_accel_free},
        _x_f{c._x_f},
        _spline{gsl_spline_alloc(gsl_interp_akima, std::get<0>(_x_f).size()), &gsl_spline_free},
        _xmin{c._xmin},
        _xmax{c._xmax} {
            gsl_spline_init(_spline.get(), std::get<0>(_x_f).data(), std::get<1>(_x_f).data(), std::get<0>(_x_f).size());
        }


    /*
     * Input:
     *    x        value of the argument
     * Output:
     *    interpolated value, if x within the interpolation domain
     *    otherwise value of the function at the closest known point
     */
    double operator()(double x) const {
        if((x < _xmax) and (x > _xmin))
            return gsl_spline_eval(_spline.get(), x, _acc.get());
        else if (x>=_xmax)
            return gsl_spline_eval(_spline.get(), _xmax, _acc.get());
        else
            return gsl_spline_eval(_spline.get(), _xmin, _acc.get());
            
    }

    // Minimum argument of the interpolation domain
    double get_xmin() const {
        return _xmin;
    }

    // Maximum argument of the interpolation domain
    double get_xmax() const {
        return _xmax;
    }

private:
    std::pair<std::vector<double>, std::vector<double>> _x_f;
    std::unique_ptr<gsl_interp_accel, decltype(&gsl_interp_accel_free)> _acc;
    std::unique_ptr<gsl_spline, decltype(&gsl_spline_free)> _spline;
    double _xmin, _xmax;
};


// From http://stackoverflow.com/a/18413206
template< typename F >
class gsl_function_pp : public gsl_function {
public:
    gsl_function_pp() {
        params = this;
    }

    void set_fun(const F& func) {
        _func = F(func);
    }
    gsl_function_pp(const F& func) : _func(func) {
        function = &gsl_function_pp::invoke;
        params=this;
    }
private:
    const F& _func;
    static double invoke(double x, void *params) {
        return static_cast<gsl_function_pp*>(params)->_func(x);
    }
};


template <class fun_type, std::size_t MEM=10000, int key=1, bool reverse=false>
class function_wrapper_1d {
public:
    function_wrapper_1d(fun_type f) : _fun{f}, w{gsl_integration_workspace_alloc(MEM)}, Fp{_fun} {
        _x.resize(MEM);
        _y.resize(MEM);
        struct {
            double operator()(){ double ret=_z; _z+=_dz; return ret; }
            double _z, _dz;
        } gtor;
        gtor._z = 0.0;
        gtor._dz = (_fun.get_xmax()-_fun.get_xmin())/(MEM-1);
        std::generate(_x.begin(), _x.end(), gtor);
        auto it = _y.begin();
        for(auto& x : _x){
            (*it)= (*this)(x);
            it++;
        } 
        _integral = interp_function{_x, _y};
    }
    double operator()(double x) const {
        double arg = reverse ? _fun.get_xmax()-(x-_fun.get_xmin()) : x-_fun.get_xmin();
        return _fun(arg);
    }

    double operator()(double x, int me){
        return (*this)(x);
    }

    double integral_slow(double time){
        
        double result, abserr;
        gsl_integration_qag(static_cast<gsl_function*>(&Fp), 0, time, 1e-5, 1e-5, MEM, key, w, &result, &abserr);

        return result;
    }

    double integral(double time){
        return _integral(time);
    }

    double get_duration(){
        return _fun.get_xmax()-_fun.get_xmin();
    }


private:
    std::vector<double> _x, _y;
    std::function<double(double)> _integral;
    const fun_type _fun;
    gsl_integration_workspace* w;
    gsl_function_pp<fun_type> Fp;
};


#endif // _INTERPOLATED_FUNCTION_HH_
