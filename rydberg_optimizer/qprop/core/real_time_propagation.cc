#include <iostream>
#include <memory>
#include <complex>
#include <vector>
#include <algorithm>
typedef std::complex<double> cplxd;
typedef std::unique_ptr<cplxd[]> cplxd_ptr;

#include <grid.h>
#include <hamop.h>
#include <wavefunction.h>
#include <parameter.hh>

#include <boost/timer.hpp>

#include <smallHelpers.hh>
#include <powers.hh>
#include <tsurffSpectrum.hh>

// Functions, which determine potentials
#include "potentials.hh"
#include "interp_function.hh"


void real_time_propagation() {
    std::ofstream outfile("out", std::ios::app);

    grid g_prop, g_load;
    wavefunction staticpot, wf, wf_load;

    // input (result from imaginary propagation)
    string str_fname_wf_ini=string("./initial_state.dat");
    FILE* file_wf_ini = fopen_with_check(str_fname_wf_ini, "r");

    int isave_wf      = 1; // 1 -- save or 0 -- don't save wavefunctions
    int iv            = 0; // verbosity of stdout

    // get parameters
    parameterListe params("simul.param");


    // *** declare the grid for load ***
    g_load.set_dim(params.getLong("qprop-dim"));
    const double delta_r = params.getDouble("delta-r");
    g_load.set_ngps(long(params.getDouble("radial-grid-size")/delta_r), 1, 1);
    g_load.set_delt(delta_r, 0.0, 0.0);
    g_load.set_offs(0, 0, 0);

    const int my_m_quantum_num = params.getLong("initial-m");

    // only A_z in this example
    interp_function vecpot_z_fun{"vecpot"};
    function_wrapper_1d<interp_function, 10000,1> vecpot_z{vecpot_z_fun};
    double pulse_duration = vecpot_z_fun.get_xmax() - vecpot_z_fun.get_xmin();

    // how long do the slowest electrons have time to reach the t-SURFF boundary
    const bool use_tsurff = params.getBool("use_tsurff");
    const double time_surff=params.getDouble("R-tsurff")/params.getDouble("p-min-tsurff");
    const double duration=pulse_duration+time_surff;

    // *** declare the grid for propagation ***
    g_prop.set_dim(params.getLong("qprop-dim"));
    const double grid_size=params.getDouble("imag-width")+params.getDouble("radial-grid-size");
    g_prop.set_ngps(long(grid_size/delta_r), params.getLong("ell-grid-size"), 1);
    g_prop.set_delt(delta_r);
    g_prop.set_offs(0, 0, 0);

    // output that will be created by hydrogen_re
    string common_prefix("real_time");
    string str_fname_logfi=common_prefix+string(".log");
    FILE* file_logfi = fopen_with_check(str_fname_logfi, "w");
    string str_fname_yield=common_prefix+string("-yield.dat");
    FILE* file_yield = fopen_with_check(str_fname_yield, "w");
    string str_fname_obser=common_prefix+string("-obser.dat");
    FILE* file_obser = fopen_with_check(str_fname_obser, "w");

    if (iv!=0) {
        fprintf(stdout, "%s will be (re)written.\n", str_fname_yield.c_str());
        fprintf(stdout, "%s will be (re)written.\n", str_fname_logfi.c_str());
    }

    // create an instance of the class for doing the tsurff related work
    std::unique_ptr<tsurffSaveWF> tsurff_save_wf_ptr;
    if(use_tsurff)
        tsurff_save_wf_ptr = std::make_unique<tsurffSaveWF>(params, g_prop);

    // the absorbing imaginary potential
    const long imag_potential_width=long(params.getDouble("imag-width")/delta_r);
    imagpot imaginarypot(imag_potential_width);
	
    // set the binding potential and the hamiltonian
    hydrogen_atom V0_H;
    lithium_sae_atom V0_Li;
    hamop hamilton;
    auto atom_type_str = params.getString("atom_type");
    if (atom_type_str == "li") {
      hamilton.init(g_prop, always_zero2{}, always_zero2{}, vecpot_z, V0_Li, always_zero5{}, always_zero5{}, imaginarypot, always_zero2{});
    } else if (atom_type_str == "h") {
      hamilton.init(g_prop, always_zero2{}, always_zero2{}, vecpot_z, V0_H, always_zero5{}, always_zero5{}, imaginarypot, always_zero2{});
    } else {
      std::cerr << "Erroneous atom type (should be either 'H' or 'Li')." << std::endl;
    }
 
    // this is the linear and constant part of the Hamiltonian
    staticpot.init(g_prop.size());
    staticpot.calculate_staticpot(g_prop, hamilton);

    // *** wavefunction array
    wf.init(g_prop.size());
    wf_load.init(g_load.size());

    // *** wavefunction initialization ***
    wf_load.init(g_load, file_wf_ini, 0, iv);
    wf.regrid(g_prop, g_load, wf_load);
	
    fclose(file_wf_ini);
	
    string str_fname_wf=common_prefix+string("-wf.dat");
    FILE* file_wf = fopen_with_check(str_fname_wf, "w");
    //wf.dump_to_file_sh(g_prop, file_wf, 1); // initial wf is saved

    const double real_timestep = params.getDouble("delta-t");
    long lno_of_ts = long( duration*1.0/real_timestep ) + 1;
    //
    // write to log file
    //
    fprintf(file_logfi, "Real-time propagation\n");
    fprintf(file_logfi, "Grid: \n");
    fprintf(file_logfi, "g_prop.ngps_x() = %ld\n", g_prop.ngps_x());
    fprintf(file_logfi, "g_prop.ngps_y() = %ld\n", g_prop.ngps_y());
    fprintf(file_logfi, "g_prop.ngps_z() = %ld\n", g_prop.ngps_z());
    fprintf(file_logfi, "g_prop.dimens() = %d\n\n", g_prop.dimens());
    fprintf(file_logfi, "g_prop.delt_x() = %15.10le\n", g_prop.delt_x());

    fprintf(file_logfi, "real_timestep     = %15.10le\n", real_timestep);
    fprintf(file_logfi, "lno_of_ts         = %ld\n", lno_of_ts);
    fprintf(file_logfi, "str_fname_wf_ini = %s\n", str_fname_wf_ini.c_str());
    fprintf(file_logfi, "str_fname_obser  = %s\n", str_fname_obser.c_str());
    if (isave_wf==1)
        fprintf(file_logfi, "str_fname_wf = %s\n", str_fname_wf.c_str());


    fflush(file_logfi);
    fclose(file_logfi);

    long ldumpwidth(1.0/real_timestep);  // output once every a.u. of time

    long save_interval = params.getDouble("save-interval");
    long wf_dumpwidth(save_interval/real_timestep);
	long wf_buffer_size = static_cast<long>(lno_of_ts/wf_dumpwidth);
	std::vector<wavefunction> wf_buffer;
	wf_buffer.push_back(wf);
	
    int me = 0; // dummy here
    // write vector potential to file
    string str_fname_vpot_z=common_prefix+string("-vpot_z.dat");
    FILE* file_vpot_z = fopen_with_check(str_fname_vpot_z, "w");
    for (long ts=0; ts<lno_of_ts; ts++) {
    const double time=real_timestep*double(ts);
    if (ts%ldumpwidth==0)
      fprintf(file_vpot_z, "%15.10le %15.10le\n", time, vecpot_z(time, me));
    };
    fclose(file_vpot_z);

    // ********************************************************
    // ***** real time propagation ****************************
    // ********************************************************


    cplxd timestep=cplxd(real_timestep, 0.0);
    cplxd P;
    double N;
    #ifdef HAVE_BOOST
    boost::timer tim;

    tim.restart();
    #endif
    for (long ts=0; ts<lno_of_ts; ts++) {

        const double time=real_timestep*double(ts);
        // save the orbitals \varphi_{\ell}(\RI) and the derivative \partial_r\varphi_{\ell}(r)|_{r=\RI}
        if( use_tsurff)
            (*tsurff_save_wf_ptr)(wf);

        if (ts%ldumpwidth==0) {
			// calculate total energy, projection onto initial state, norm, and <z>
			double E_tot = real(wf.energy(0.0, g_prop, hamilton, me, staticpot, 1.0));
			P = wf.project(g_prop, g_load, wf_load, 0);
			N = wf.norm(g_prop);
			double z_expect = real(wf.expect_z(g_prop));
			fprintf(file_obser, "%15.10le %15.10le %15.10le %15.10le %15.10le\n", time, E_tot, real(conj(P)*P), N, z_expect);
			wf_buffer.push_back(wf);
        }
        //
        // propagate one step forward in (real) time.
        //
        wf.propagate(timestep, time, g_prop, hamilton, me, staticpot, my_m_quantum_num, 1.0);

        if((wf_dumpwidth > 0) and (ts%wf_dumpwidth==0)){
            std::string intermediate_wf_fname = common_prefix + std::string("-wf.dat_") + std::to_string(ts);

            FILE* file_wf = fopen_with_check(intermediate_wf_fname, "w");
            wf.dump_to_file_sh(g_prop, file_wf, 1);
            fclose(file_wf);
        }

        if (ts%(ldumpwidth*10)==0) {
          outfile << "timestep " << ts << " of " << lno_of_ts << ", Norm of wave function: " << N << endl;

        #ifdef HAVE_BOOST
        outfile << "    ETA: " << static_cast<int>((tim.elapsed()/ts*(lno_of_ts-ts))/60.0) << " minutes" << endl;
        #endif
        }
    } // end of real-time-propagation loop

    fclose(file_obser);

    double yield_N = (1.0 - N);
    double yield_P = (1.0 - real(conj(P)*P));
    fprintf(file_yield, "%15.10le %15.10le\n", yield_N, yield_P);
    fclose(file_yield);

    wf.dump_to_file_sh(g_prop, file_wf, 1); // final wf is saved
    fclose(file_wf);

    if (iv!=0) {
        fprintf(stdout, "%s was read.\n",   str_fname_wf_ini.c_str());
        fprintf(stdout, "%s is written.\n", str_fname_obser.c_str());
        fprintf(stdout, "%s is written.\n", str_fname_wf.c_str());
        fprintf(stdout, "%s is written.\n", str_fname_vpot_z.c_str());
        fprintf(stdout, "%s is written.\n", str_fname_logfi.c_str());
        fprintf(stdout, "%s is written.\n", str_fname_yield.c_str());
        fprintf(stdout, "Hasta la vista...\n");
    }
	
}
