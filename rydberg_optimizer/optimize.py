#!/usr/bin/env python3
from rydberg_optimizer.common.utils import mkdir_p
from rydberg_optimizer.common.constraints import *
from rydberg_optimizer.common.common_setup import setup_constraints_and_optimizers, get_allowed_initial_quess
from rydberg_optimizer.qprop import *
from rydberg_optimizer.states.rydberg_states import RydbergStates
from numpy import sqrt, power, linspace, savetxt, array
import random
import shutil
import nlopt
import os
import errno
import logging

def optimize_main():
    # Get the name and extension of this program
    name, extension = os.path.splitext(os.path.basename(sys.argv[0]))

    # Parse the input file
    simulation_config, laser, oct_config, constraints, variable_limits, initial_value = get_config()

    # Set up logging
    if simulation_config['log_to_file']:
        logging.basicConfig(filename=name + ".log",
                            level=logging.DEBUG, format='%(message)s')
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(message)s')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
    else:
        logging.basicConfig(level=logging.DEBUG, format='%(message)s')

    logger = logging.getLogger('')
    
    mkdir_p("opt_out")
    mkdir_p("simulation")
    shutil.copyfile('input', 'opt_out/input')
    # Set up the container we use to pass information between the target and
    # the constraint functions
    result_container = simulation_result_container(
        simulation_config['ell-grid-size'])
    
    rydberg_states = RydbergStates()
    # Select the target function
    if (oct_config['target'] == "rydberg"):
        def target(x, grad): return target_rydberg(
            x, grad, laser, result_container, simulation_config,
            oct_config['states'], rydberg_states)
    elif oct_config['target'] == 'pes':
        def target(x, grad): return target_rydberg(x, grad, laser, result_container,
                                                   simulation_config,
                                                   oct_config['energyrange'][0],
                                                   tconfig['energyrange'][1],
                                                   rydberg_states)

    opt, constr_funs = setup_constraints_and_optimizers(
        oct_config, constraints, laser, variable_limits, result_container)

    opt.set_max_objective(target)
    if(oct_config['max_iterations'] > 0):
        opt.set_maxeval(oct_config['max_iterations'])

    if (len(initial_value) == 0):
        logger.info("We don't have any parameters to optimize, exiting.")
    else:
        initial_value = get_allowed_initial_quess(
            laser, variable_limits, constr_funs)

        # Calculate GS
        simulation_config['vecpot-file'] = 'empty'
        qprop_write_input_file(simulation_config)
        qprop_prepare_initial_state(simulation_config, rydberg_states)

        xopt = opt.optimize(initial_value)

        logger.info("Final params:")
        laser.set_vars(xopt)
        laser.print_parameters()

if __name__=='__main__':
    optimize_main()
