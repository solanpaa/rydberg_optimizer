import os, warnings, dill, binascii, filelock
from scipy.special import genlaguerre
from scipy.integrate import simps
from scipy.interpolate import interp1d
from .radial_tise_solver import get_eigensystem
import numpy as np
import h5py
import copy
import logging

class RydbergStates(object):
    
    def __init__(self, cache_path =
                 '~/.cache/rydberg_optimizer/'):
        self._cache_path = os.path.expanduser(cache_path)
        self._rydbergstates = {} # key is (n,l)
        self._cache_loaded = False
        self._energies = {} # key is (n,l)

    def _calculate_hydrogen_rydberg_states(self, states, dr, Rmax):
        """
        Calculates hydrogen eigenstates with (n,l) given by the list states. 
        l-quantum numbers calculated only up to Lmax. Radial grid will be [dr, 2dr, ..., Rmax]
        """
        r_npts = int(Rmax/dr)
        r = np.arange(dr, r_npts*dr+0.5*dr, dr)
        for n,l in states:
            self._rydbergstates[n, l] = _hydrogen_radial_wf(n, l, r)
            self._energies[n,l] = -1/(2*n**2)

    def _load_cached_states(self, atom_type, states, dr, Rmax):
        """
        Loads SAE rydberg states we need if possible.
        """
        filepath = self._cache_path + "/"+ atom_type.lower() + ".h5" 
        r_npts = int(Rmax/dr)
        rgrid = np.arange(dr, r_npts*dr+0.5*dr, dr)

        # Calculate the highest L we actually need to have in memory
        
        try:
            lock = filelock.FileLock(filepath+".lock")
            with lock:
                with h5py.File(filepath,'r') as db:
                    db_rgrid = db['radial_grid']
                    dr_file = db_rgrid[1]-db_rgrid[0]
                    if dr < dr_file:
                        raise RuntimeWarning("Cache for %s rydberg states exists\
            but has too large grid spacing."%atom_type)
                    if db_rgrid[-1] < rgrid[-1] :
                        raise RuntimeWarning("Cache for %s rydberg states exists\
            but has too small grid."%atom_type)

                    missing_states = []

                    need_to_downsample = dr != dr_file or rgrid[-1] > db_rgrid[-1]

                    for n,l in states:
                        try:
                            if need_to_downsample:
                                self._rydbergstates[n,l] = np.interp(rgrid, db_rgrid,
                                                                 db['state_n%d_l%d'%(n,l)][:])
                            else:
                                self._rydbergstates[n,l] = db['state_n%d_l%d'%(n,l)][:]

                        except:
                            missing_states.append((n,l))

                    self._energies = dill.loads(binascii.unhexlify(db['energies'].value.encode('utf-8')))

                    self._cache_loaded = True
                    return missing_states

        except:
            warnings.warn("No states cache found.")
            return states

    def save_to_cache(self):
        atom_type = self.atom_type
        dr = self.dr 
        Rmax = self.Rmax
        os.makedirs(self._cache_path, exist_ok=True)
        lock = filelock.FileLock( self._cache_path + "/%s.h5.lock"%atom_type)
        with lock:
            if os.path.exists( self._cache_path+"/%s.h5" % atom_type ):
                with h5py.File( self._cache_path+"/%s.h5" % atom_type,
                               'r') as db:
                    rgrid_old = db['radial_grid']
                    Rmax_old = rgrid_old[-1]
                    dr_old = rgrid_old[1]-rgrid_old[0]
                    if dr_old < dr or Rmax_old > Rmax: # new states are somehow worse
                        return
            self._save_to_cache_core(atom_type, dr, Rmax)
        
    def _save_to_cache_core(self, atom_type, dr, Rmax):
        """This should be always called under filelock"""
        try:

            os.remove(self._cache_path+"/%s_states.h5" % atom_type)
        except:
            pass
        r_npts = int(Rmax/dr)
        r = np.arange(dr, r_npts*dr+0.5*dr, dr)
        
        mkdir_p(self._cache_path)
        with h5py.File(self._cache_path+"/%s.h5" %
                       atom_type.lower(),
                                               'w') as db:
            db['energies'] = binascii.hexlify(dill.dumps(self._energies)).decode("utf-8")
            db['radial_grid'] = r
            for key, wf in self._rydbergstates.items():
                db["state_n%d_l%d"%(key[0], key[1])] = wf


    def get_state(self, sconfig, n,l):

        assert l <= sconfig['ell-grid-size'], "Requested initial state with\
                too high angular momentum compared to l-grid"


        self._calculate_rydberg_states_core(sconfig['atom-type'].lower(),
                                            [(n,l)], sconfig['delta-r'],
                                            sconfig['radial-grid-length'] +
                                            sconfig['imag-width'])

        return self._rydbergstates[n,l]


    def _calculate_rydberg_states_core(self, atom_type, states, dr,
                                       Rmax):
        """
        Loads or calculates radial eigenstates of atoms with SAE potential.
        """
        self.dr = dr
        self.Rmax = Rmax
        self.atom_type = atom_type
        # Handle Hydrogen separately since for that we have an analytical solution
        if atom_type == 'h':
            self._calculate_hydrogen_rydberg_states(states, dr, Rmax)
            return
        logger = logging.getLogger('')
   
        try: 
            missing_states = self._load_cached_states(atom_type, states, dr,
                                                      Rmax)
        except Exception as e:
            logger.info("Could not load cached states: "+repr(e))
            missing_states = states

        if len(missing_states) == 0:
            return

        # Select SAE potential
        if atom_type == 'li':
            sae_potential = _potential_Li_sae
        else:
            raise ValueError("No such atomic SAE potential implemented: %s"%atom_type)

        gridpoints = int(Rmax / dr)
        r_npts = int(Rmax/dr)
        r = np.arange(dr, r_npts*dr+0.5*dr, dr)
        
        # Calculate the states that are missing
        lvalues = np.unique([l for n,l in missing_states])
            
        logger = logging.getLogger('')
        for l in lvalues:
            nmin = l+1
            nmax = max(missing_states, key=lambda s: s[0])[0]
            n_states = nmax-nmin+1
            logger = logging.getLogger('')
            
            logger.info("Calculating Rydberg states: n = %d .. %d, l=%d"%(nmin,
                nmax, l))  
            def potential(r):
                return sae_potential(r)+l*(l+1)/(2*r**2)
            evals, evecs = get_eigensystem(r, potential, how_many = n_states)
            for idx, n in enumerate(np.arange(nmin, nmax+1, dtype=int)):
                self._rydbergstates[n,l] = evecs[:, idx]
                self._energies[n,l] = np.real(evals[idx])

        self.save_to_cache()

    def calculate_rydberg_states(self, sconfig, states):
        """
        Calculates or loads radial parts of rydberg states for various
        SAE atomic potentials.
        """
        Rmax = sconfig['radial-grid-length'] + sconfig['imag-width']
        dr = sconfig['delta-r']
        Lmax = sconfig['ell-grid-max-size']
        atom_type = sconfig['atom-type'].lower()

        states = filter_high_l_not_in_simulation(states, Lmax)
        self._calculate_rydberg_states_core(atom_type, states, dr, Rmax)
        # Save the calculated values to cache (if we have better grid)
        if atom_type.lower() != 'h':
            self.save_to_cache()

    def calculate_overlaps(self, sconfig, states, path='simulation/real_time-wf.dat'):
        """
        Calculates and returns the overlap matrix |<n,l|ψ >|^2
        """

        # Load wf
        data = np.loadtxt(path)
        # Combine the real and imaginary parts
        wfdata = data[:,0] + 1j*data[:,1]
        
        r_npts = int((sconfig['radial-grid-length']+sconfig['imag-width'])/(sconfig['delta-r']))
        ell_npts = int(len(wfdata)/r_npts)
        Lmax = ell_npts-1
        # Split the wavefunction into list of radial parts for different angular
        # momenta
        wf_lrepr = np.split(wfdata, ell_npts)
        states = filter_high_l_not_in_simulation(states, Lmax)

        # Generate Rydberg states if needed
        for s in states:
            if not s in self._rydbergstates.keys():
                self.calculate_rydberg_states(sconfig, states)

        # Calculate overlaps
        S = {}
        total_overlap = 0
        for n,l in states:
            S[n,l] = np.abs(simps(self._rydbergstates[n, l] * wf_lrepr[l], dx=float(sconfig['delta-r'])))**2
            total_overlap += S[n,l]

        return S, total_overlap

    def get_energies(self, sconfig, states):
        for s in states:
            if not s in self._energies.keys():
                self.calculate_rydberg_states(sconfig, states)

        return self._energies

def _hydrogen_radial_wf(n, l, r):
    """
    Returns the radial part of the hydrogen eigenstate,
    with principal quantum number n, angular momentum quantum number l,
    at radii r.
    """
    return r * genlaguerre(n - l - 1, 2 * l + 1)(2 * r / n) * np.exp(-r / n) * (2 * r / n)**l * np.sqrt((2.0 / n)**3 * np.math.factorial(n - l - 1.0) / (2.0 * n * np.math.factorial(n + l)))

def _potential_Li_sae(r):
    """Returns the SAE potential for Li"""
    a1 = 3.395;
    a2 = 3.212;
    a3 = 3.207;
    Z  = 3.0;
    Zt = 1.0;
    return -1.0/r*(Zt+(Z-Zt)*np.exp(-a1*r)+a2*r*np.exp(-a3*r))

def mkdir_p(path):
    try:
        os.makedirs(path)
    except:
        pass

def filter_high_l_not_in_simulation(states, Lmax):
        max_requested_L = max(states, key = lambda s: s[1])[1]
        if max_requested_L > Lmax:
            warnings.warn("""Requested overlaps with high-l states not in the
                          simluation""")
            states_tmp = copy.deepcopy(states)
            for n,l in states: 
                if l > Lmax:
                    states_tmp.remove((n,l))

        try:
            return states_tmp
        except:
            return states
        
