#!/usr/bin/env python3

"""
Extracted solution of ex4.4, solving eigenstates of 1D hydrogen.
"""

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps

def get_eigensystem(grid, potential, how_many=1):
    """
    Calculates the eigensolutions of the radial time-independent Schrödinger
    equation.

    Parameters
    ----------

    grid : np.ndarray of floats, shape (N)
        The real space coordinate grid, should be uniform.
    potential : callable
        Function returning the model potential V(x). This function should take
        np.ndarray (`grid`) as the argument and return the potential values
        at these coordinates.

    Returns
    -------

    psi : np.ndarray of floats or complex numbers, shape (N)
        The ground state wavefunction on the coordinate grid.
    """

    grid_size = grid.shape[0]
    dx = grid[1] - grid[0]
    dx2 = dx * dx

    diagonal = 1 / dx2 * np.ones(grid_size) + potential(grid)
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            -0.5 / dx2 * np.ones(grid_size - 1),
            diagonal
        ],
        [-1, 1, 0],
        format="dia")


    # Diagonalize (note that our matrix is real symmetric so we use the
    # `eigsh`-routine instead of `eigs`).
    evals, evecs = sla.eigsh(H0, k=how_many, which='SA', maxiter=grid_size*100)

    # Normalize the eigenvector
    for i in range(evecs.shape[1]):
        evecs[:,i] = evecs[:,i]/np.sqrt( simps(np.abs(evecs[:,i])**2,
                                               x=grid))

    return evals, evecs
