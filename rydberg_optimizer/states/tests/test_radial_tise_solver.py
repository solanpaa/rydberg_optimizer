import unittest

import numpy as np
from ..radial_tise_solver import *
from ..rydberg_states import _hydrogen_radial_wf

@unittest.SkipTest
class TestRadialSolver(unittest.TestCase):
    def test_hydrogen_l0(self):
        """Test Hydrogen l=0 lowest states"""
        dr = 0.01
        r = np.arange(dr, 50, dr)

        # The effective potential
        def potential(r, l):
            return -1 / r + l * (l + 1) / (2 * r * r)

        evals, evecs = get_eigensystem(r, lambda x: potential(x, 0), 2)
        self.assertAlmostEqual(evals[0], -0.5, places=3)
        self.assertAlmostEqual(evals[1], -1 / 8, places=3)
        np.testing.assert_allclose(np.fabs(evecs[:, 0]),
                                   np.fabs(_hydrogen_radial_wf(1, 0, r)),
                                   atol=1e-4, rtol=0)

        np.testing.assert_allclose(np.fabs(evecs[:, 1]),
                                   np.fabs(_hydrogen_radial_wf(2, 0, r)),
                                   atol=1e-4, rtol=0)

    def test_hydrogen_l1(self):
        """Test Hydrogen l=1 lowest states"""
        dr = 0.01
        r = np.arange(dr, 50, dr)

        # The effective potential
        def potential(r, l):
            return -1 / r + l * (l + 1) / (2 * r * r)

        evals, evecs = get_eigensystem(r, lambda x: potential(x, 1), 2)

        self.assertAlmostEqual(evals[0], -1 / 8, places=3)
        self.assertAlmostEqual(evals[1], -1 / (2 * 3 * 3), places=3)
        np.testing.assert_allclose(np.fabs(evecs[:, 0]),
                                   np.fabs(_hydrogen_radial_wf(2, 1, r)),
                                   atol=1e-4, rtol=0)

        np.testing.assert_allclose(np.fabs(evecs[:, 1]),
                                   np.fabs(_hydrogen_radial_wf(3, 1, r)),
                                   atol=1e-3, rtol=0)
