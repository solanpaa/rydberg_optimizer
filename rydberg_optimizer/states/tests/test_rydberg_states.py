import unittest
import os
import shutil
import numpy as np
from ..rydberg_states import RydbergStates, _hydrogen_radial_wf
import h5py


class TestRydbergStates(unittest.TestCase):
    def test_calculate_hydrogen_rydberg_states(self):
        """ Test that precalculation of hydrogen rydberg states works"""

        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)

        states = [(1,0), (5,1), (6,2), (3,2)]
        dr = 0.05
        Rmax = 20
        rs._calculate_hydrogen_rydberg_states(states, dr, Rmax)


        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)

        self.assertFalse(os.path.exists(rs._cache_path + "/h.h5"))

    def test_calculate_lithium_rydberg_states(self):
        """ Test that precalculation of lithium rydberg states works"""
        # Remove test cache
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)

        states = [ (3,1), (2,1), (1,0)]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)
        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)

    def test_calculate_lithium_rydberg_states_caching(self):
        """ Test caching of lithium rydberg states"""
        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [(2,0), (1,0), (5,3), (5,4)]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)

        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        with h5py.File(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5"),
                       'r') as cfile:
            for n,l in states:
                self.assertTrue('state_n%d_l%d' %
                                    (n, l) in list(cfile.keys()))

    def test_loading_cache(self):
        """ Test loading the cache of previously saved states """

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [(1,0), (2,0), (4,0), (3,2), (2,1)]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)

        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        del rs

        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        rs._load_cached_states('li', states, dr, Rmax)
        self.assertTrue(rs._cache_loaded)
        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)

    def test_loading_cache_correct_states(self):
        """ Test loading the cache of previously saved states loads correct states"""

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [(1,0), (2,0), (4,0), (3,2), (2,1)]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)

        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))


        rsn = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        rsn._load_cached_states('li', states, dr, Rmax)
        self.assertTrue(rsn._cache_loaded)
        for n,l in states:
            self.assertTrue((n, l) in rsn._rydbergstates)

        for n,l in states:
            np.testing.assert_allclose(rs._rydbergstates[n,l],
                                       rsn._rydbergstates[n,l])


    def test_loading_cache_missing_states(self):
        """ Test loading the cache of previously saved states but now we want more """

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [(1,0), (2,0), (4,0), (3,2), (2,1)]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)

        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        del rs
        states = [(1,0), (2,0), (4,0), (3,2), (2,1), (5,0), (5,4)]
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)
        self.assertTrue(rs._cache_loaded)
        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)



    def test_loading_cache_resample1(self):
        """ Test loading the cache of previously saved states and resampling """

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [ (1,0), (2,0), (4,2) ]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)
        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)
        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        del rs
        dr = 0.1
        Rmax = 20

        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        rs_expected = RydbergStates(
            cache_path='~/.cache/rydberg_optimizer/tests')
        rs_expected._calculate_rydberg_states_core(
            'li', states, dr, Rmax)

        rs._load_cached_states('li', states, dr, Rmax)
        for n,l in states:
            np.testing.assert_allclose(np.fabs(rs._rydbergstates[n, l]),
                                           np.fabs(rs_expected._rydbergstates[n, l]))

    def test_loading_cache_resample2(self):
        """ Test loading the cache of previously saved states and resampling """

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [ (1,0), (2,0), (4,2) ]
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states, dr, Rmax)
        
        for n,l in states:
            self.assertTrue((n, l) in rs._rydbergstates)
        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        del rs
        states = [ (1,0), (2,0), (4,2) ]
        dr = 0.05
        Rmax = 15

        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        rs_expected = RydbergStates(
            cache_path='~/.cache/rydberg_optimizer/tests')
        rs_expected._calculate_rydberg_states_core(
            'li', states, dr, Rmax)

        rs._load_cached_states('li', states, dr, Rmax)
        for n,l in states:
            np.testing.assert_allclose(np.fabs(rs._rydbergstates[n, l]),
                                           np.fabs(rs_expected._rydbergstates[n, l]))

    def test_loading_cache_resample3(self):
        """ Test loading the cache of previously saved states and resampling """

        # Remove test cache if exists
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        shutil.rmtree(rs._cache_path, ignore_errors=True)
        states = [(1,0), (5,0), (3,0), (2,0), (3,1)] 
        dr = 0.05
        Rmax = 20
        rs._calculate_rydberg_states_core('li', states,dr, Rmax)

        rs.save_to_cache()
        # Now we should have a cache
        self.assertTrue(
            os.path.exists(os.path.expanduser("~/.cache/rydberg_optimizer/tests/li.h5")))

        del rs
        states = [(1,0), (2,1), (5,3), (3,2), (6,0)]
        dr = 0.05
        Rmax = 15

        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')

        missing_states = rs._load_cached_states(
            'li', states, dr, Rmax)

        for s in missing_states:
            self.assertTrue(s in [(5,3), (3,2), (6,0)])

    def test_overlaps(self):
        """Test calculating the overlaps"""
        rs = RydbergStates(cache_path='~/.cache/rydberg_optimizer/tests')
        sconfig = {'atom-type' : 'H',
                   'radial-grid-length' : 100,
                   'imag-width' : 10,
                   'delta-r' : 0.01,
                   'ell-grid-max-size' : 5}

        states = [ (1,0), (2,1), (3,1), (3,2), (4,1), (4,0), (4,2), (5,3),
                  (5,4)]
        rs.calculate_rydberg_states(sconfig, states)

        # Create a test state
        r_npts = int(rs.Rmax/rs.dr)
        r = np.arange(rs.dr, r_npts*rs.dr+0.5*rs.dr, rs.dr)
        psi0 = np.concatenate([ 
                np.zeros_like(r),
                1/21.*_hydrogen_radial_wf(2,1,r)+1/17.*_hydrogen_radial_wf(4,1,r),
                1/6.*_hydrogen_radial_wf(3,2,r),
                (1/21.-1j/8)*_hydrogen_radial_wf(5,3,r),
               ])
        data = np.vstack([psi0.real, psi0.imag]).T
        np.savetxt("tmp_teststate", data)

        S, total_overlap = rs.calculate_overlaps(sconfig, states, "tmp_teststate")
        os.remove("tmp_teststate")

        self.assertAlmostEqual(S[2,1], 1/21**2)
        self.assertAlmostEqual(S[4,1], 1/17**2)
        self.assertAlmostEqual(S[1,0], 0)
        self.assertAlmostEqual(S[3,1], 0)
        self.assertAlmostEqual(S[3,2], 1/6**2)
        self.assertAlmostEqual(S[5,3], np.abs(1/21.-1j/8)**2)
        self.assertAlmostEqual(S[4,2], 0)
        self.assertFalse( (5,4) in S)


