#!/usr/bin/env python3
from rydberg_optimizer.common import *
import numpy as np
import scipy.special as sp

def read_qprop_wf_xy_cut(filename):
    # Read the simulation config
    simulation_config, _,_,_,_,_ = get_config()
    
    # Load the saved wavefunction
    rdata = np.loadtxt(filename)
    wfdata = rdata[:,0] + 1j*rdata[:,1]

    # Number of radial gridpoints
    r_npts = int((simulation_config['radial-grid-length']+simulation_config['imag-width'])/(simulation_config['delta-r']))

    # Number of l-values
    ell_npts = int(len(wfdata)/r_npts) # Not from config as this can change if the simulation doesn't converge
    
    #Set the ranges of variables for graphing
    phi_number = 1000
    theta0 = np.pi/2
    theta1 = np.pi*3/2

    # Radial and angular grids
    r_space = np.arange(0, int(simulation_config['radial-grid-length'])/(simulation_config['delta-r']), dtype=int)
    phi_space = np.linspace(0, np.pi, phi_number)
    R, PHI = np.meshgrid(r_space, phi_space)

    # Gridpoints in cartesian coordinates
    X1, Y1 = ((R+1)*(simulation_config['delta-r']))*np.sin(PHI)*np.sin(theta0), (R+1)*(simulation_config['delta-r'])*np.cos(PHI)
    X2, Y2 = ((R+1)*(simulation_config['delta-r']))*np.sin(PHI)*np.sin(theta1), (R+1)*(simulation_config['delta-r'])*np.cos(PHI)

    Z1 = np.zeros(((len(phi_space)),(len(r_space))), dtype=complex)
    Z2 = np.zeros(((len(phi_space)),(len(r_space))), dtype=complex)
    for ell in range(ell_npts):
        Z1 = Z1 + (wfdata[(R+ell*(r_npts))]/((R+1)*(simulation_config['delta-r']))*sp.sph_harm(0, ell, theta0, PHI))
        Z2 = Z2 + (wfdata[(R+ell*(r_npts))]/((R+1)*(simulation_config['delta-r']))*sp.sph_harm(0, ell, theta1, PHI))

    # Combine X1,X2, Y1,Y2, Z2,Z2 and return
    return np.vstack((X1,X2)), np.vstack((Y1,Y2)), np.vstack((Z1,Z2))
