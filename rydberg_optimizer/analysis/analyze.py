#!/usr/bin/env python3

import glob
from copy import deepcopy
import shutil

import matplotlib
matplotlib.use('Qt4Agg')

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from PyQt4 import QtGui, QtCore

from scipy.integrate import simps
from numpy import *
from rydberg_optimizer.common.constraints import *
from rydberg_optimizer.common.parser import *
from rydberg_optimizer.qprop import *
from rydberg_optimizer.common.utils import mkdir_p
from rydberg_optimizer.states.rydberg_states import RydbergStates
from rydberg_optimizer.analysis.plot_final_state_overlaps import _plot_overlaps_core


class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        if( len(sys.argv) == 2):
            self.path = sys.argv[1]
        else:
            self.path = "./"

        # Read the input information for the optimization simulatios
        self.sconfig, _, self.tconfig, self.constraints, _, _ = get_config(self.path)
        self.rs = RydbergStates()

        # Create figure and subplots
        self.figure = plt.figure(figsize=(6,10))

        #  An axes for plotting the constraints
        self.ax_constrained = self.figure.add_subplot(311)
        self.ax_constrained.axhline(y=0, color='black')
        self.ax_constrained.set_ylabel('Constr (<0 ok)')

        # Axes for plotting the target values
        self.ax_target= self.figure.add_subplot(312)
        self.ax_target.set_ylabel('Target', color='b')
        self.ax_ionization = self.ax_target.twinx()
        self.ax_ionization.set_ylabel('Ionization', color='r')


        # Axes for showing select laser pulses
        self.ax_laser = self.figure.add_subplot(325)
        self.ax_laser.set_xlabel('time')
        self.ax_laser.set_ylabel('E-field')

        # Axes for showing occupations for select laser pulses
        self.ax_occupations = self.figure.add_subplot(326, projection='3d')
        self.ax_occupations.set_xlabel(r'$n$', labelpad=10)
        self.ax_occupations.set_ylabel(r'$l$', labelpad=10)

        # Files for lasers and the iteration output
        self.laser_files = sort(glob.glob(self.path+'/laser*'))
        self.result_files = sort(glob.glob(self.path+'/iter_out_*'))

        self.num_iterations = len(self.result_files)

        # Calculate and plot the target values
        self.result_data = {'overlap' : zeros(self.num_iterations),
                            'ionization' : zeros(self.num_iterations),
                            'Emax' : zeros(self.num_iterations),
                            'fluence' : zeros(self.num_iterations),
                            'laser integral' : zeros(self.num_iterations),
                            'field fwhm' : zeros(self.num_iterations),
                            'intensity fwhm' : zeros(self.num_iterations),
                            'duration' : zeros(self.num_iterations)}

        self.load_data()

        self.plot_target()

        # Calculate and plot the constraints
        self.constraint_data = {}
        self.plot_constraintvalues()

        # Dictionary for storing plots of individual iterations
        self.iter_plots = {}

        self.canvas = FigureCanvas(self.figure)
        self.canvas.draw()

        self.toolbar = NavigationToolbar(self.canvas, self)

        self.button = QtGui.QPushButton('Plot')
        self.button.pressed.connect(self.plot_iter)

        # iterpos shows the position of the current selection line
        self.iterpos = 0
        # A vertical line in the target graph
        self.itervline1 = self.ax_target.axvline(x=self.iterpos)
        # .. and in the constraint graph
        self.itervline2 = self.ax_constrained.axvline(x=self.iterpos)

        # A slider for selecting the iteration for closer inspection
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.slider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.slider.setGeometry(0, 40, 100, 30)
        self.slider.valueChanged[int].connect(self.change_iter_vline)
        self.slider.setSingleStep(1)
        self.slider.setMinimum(0)
        self.slider.setMaximum(self.num_iterations-1)

        # Layout
        layout = QtGui.QVBoxLayout()

        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.slider)
        layout.addWidget(self.button)

        # Sort the iteration info list
        layout2 = QtGui.QHBoxLayout()
        self.iterlist= QtGui.QListWidget()
        self.iterlist.setSortingEnabled(True)
        self.iterlist.clicked.connect(self.show_pulse_info)
        self.iterlist.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        layout2.addWidget(self.iterlist)
        self.pulseinfo = QtGui.QTextEdit()
        self.pulseinfo.setReadOnly(True)
        layout2.addWidget(self.pulseinfo)
        layout.addLayout(layout2)

        layout3 = QtGui.QHBoxLayout()
        self.removepulsebutton = QtGui.QPushButton('Remove')
        self.removepulsebutton.clicked.connect(self.remove_pulse)

        self.exportplottedbutton = QtGui.QPushButton('Export')
        self.exportplottedbutton.clicked.connect(self.export_plotted)

        self.best_10_button = QtGui.QPushButton('Best 10')
        self.best_10_button.clicked.connect(self.get_best10)

        layout3.addWidget(self.removepulsebutton) #removes the pulse selected in iterlist from plots
        layout3.addWidget(self.exportplottedbutton) #exports the laser and pes files of the plotted pulses (copies them, no renaming)
        layout3.addWidget(self.best_10_button)
        layout.addLayout(layout3)

        self.setLayout(layout)
        self.figure.tight_layout(pad=1)

        # Adjustments
        #self.figure.subplots_adjust(bottom=0.15, hspace=0.3, wspace=0.3)

    def load_data(self):
        for i in range(self.num_iterations):
            parser = configparser.SafeConfigParser()
            parser.read(self.result_files[i])
            self.result_data['overlap'][i] = parser.getfloat("Output", "overlap")
            self.result_data['ionization'][i] = parser.getfloat("Output", "ionization")
            self.result_data['Emax'][i] = parser.getfloat("Output", "laser-max-amplitude")
            self.result_data['fluence'][i] = parser.getfloat("Output", "laser-fluence")
            self.result_data['laser integral'][i] = parser.getfloat("Output", "laser-integral")
            self.result_data['field fwhm'][i] = parser.getfloat("Output", "laser-duration-fwhm")
            self.result_data['intensity fwhm'][i] = parser.getfloat("Output", "laser-duration-intensity-fwhm")
            self.result_data['duration'][i] = parser.getfloat("Output", "laser-duration")

    def show_pulse_info(self):
        self.pulseinfo.clear()
        for i in range(self.iterlist.count()):
            if self.iterlist.isItemSelected(self.iterlist.item(i)):
                self.pulseinfo.append(self.iter_plots[int(str(self.iterlist.item(i).text()))][2])


    def remove_pulse(self):
        toberemoved = []
        for i in range(self.iterlist.count()):
            if self.iterlist.isItemSelected(self.iterlist.item(i)):
                toberemoved.append(i)
        toberemoved=sort(toberemoved)
        for i in toberemoved[::-1]:
            plots = self.iter_plots.pop(int(str(self.iterlist.item(i).text())), None)
            tmp = self.iterlist.takeItem(i)
            tmp = None
            plots[0][0].remove()
            plots[1].remove()
        if(not self.iter_plots):
            self.ax_laser.legend_.remove()
        else:
            self.ax_laser.legend(fontsize=8)
        self.canvas.draw()

        self.show_pulse_info()


    def export_plotted(self):
        mkdir_p(self.path+"/export")
        tobeexported= []
        for i in range(self.iterlist.count()):
            if self.iterlist.isItemSelected(self.iterlist.item(i)):
                tobeexported.append(int(str(self.iterlist.item(i).text())))
        for i in tobeexported:
            shutil.copyfile(self.laser_files[i], self.path+ "/export/laser_%d"%i)
            shutil.copyfile(self.result_files[i], self.path+"/export/result_%d"%i)


    def change_iter_vline(self, i):
        self.itervline1.remove()
        self.itervline2.remove()
        self.itervline1 = self.ax_target.axvline(x=i, color='black')
        self.itervline2 = self.ax_constrained.axvline(x=i, color='black')

        self.iterpos = i
        self.canvas.draw()

    def plot_constraintvalues(self):

        if "maxduration" in self.constraints:
            mdur = float(self.constraints["maxduration"])
            self.constraint_data["maxduration"] = (self.result_data['duration']-mdur)/mdur
            self.ax_constrained.plot(self.constraint_data["maxduration"], label='Duration')
        if "maxfield" in self.constraints:
            mfi = float(self.constraints["maxfield"])
            self.constraint_data["maxfield"] = (self.result_data['Emax']-mfi)/mfi
            self.ax_constrained.plot(self.constraint_data["maxfield"], label='Field')
        if "maxfluence" in self.constraints:
            mfl = float(self.constraints["maxfluence"])
            self.constraint_data["maxfluence"] = (self.result_data['fluence']-mfl)/mfl
            self.ax_constrained.plot(self.constraint_data["maxfluence"], label='Fluence')

        self.ax_constrained.legend(fontsize=8)



    def plot_target(self):

        self.ax_target.plot(self.result_data['overlap'])
        self.ax_ionization.plot(self.result_data['ionization'],'r')
        # refresh canvas


    def get_best10(self):
        constraint_ok = full(self.num_iterations, True, dtype=bool)
        for key, data in self.constraint_data.items():
            constraint_ok = logical_and(constraint_ok, data<0)

        tmp = deepcopy(self.result_data['overlap'])
        tmp[~constraint_ok] = 0

        maxbest = np.min([10, len(self.result_data)])
        max10iters = np.argpartition(-tmp, -maxbest)[-maxbest:]

        for i in max10iters:
            if( tmp[i] > 0):
                self.plot_iter(i)

    def plot_iter(self, i=-1):
        if not isinstance(i, int):
            i = self.slider.value()
        if i< 0:
            i = self.slider.value()
        if i not in self.iter_plots:
            laser = loadtxt( self.laser_files[i] )

            # plot data
            infotext = u"Iter %d\n"%i
            lf = open( self.laser_files[i] )
            line = lf.readline()
            while(line[0] == "#"):
                infotext+=line[1:]
                line = lf.readline()
            lf.close()
            S, total_overlap = self.rs.calculate_overlaps(self.sconfig,
                                                                  self.tconfig['states'], 
                                                                  self.path+"/real_time-wf.dat_%0.6d"%i)
            laserplot = self.ax_laser.plot(laser[:,0],laser[:,1], label='%d'%i)

            self.iter_plots[i] = (laserplot,
                                  _plot_overlaps_core(self.ax_occupations, S,
                                                      color=laserplot[0].get_color(),
                                                      edgecolor=(1,1,1,0.2)),
                                  infotext)

            self.ax_laser.relim()
            self.ax_laser.autoscale_view()
            self.ax_occupations.autoscale_view()
            self.iterlist.addItem("%d"%i)

            self.ax_laser.legend(fontsize=8)

            # refresh canvas
        self.canvas.draw()
        self.canvas.draw()


def run_optimization_analysis():
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    run_optimization_analysis()
