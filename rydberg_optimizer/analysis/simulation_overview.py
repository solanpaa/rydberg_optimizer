"""
Overview of a single-shot simulation
"""

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
from matplotlib.patches import Rectangle
import matplotlib
from scipy.signal import stft
from matplotlib.ticker import FuncFormatter
from rydberg_optimizer.common.parser import parse_laserfile, _list_hydrogenic_states
from rydberg_optimizer.analysis.populations_graph import calculate_populations
import argparse
import os

def nice_formatter(x, pos):
    if x == 0:
        return r"$0$"

    frac_, int_ = np.modf(x)
    int_ = int(int_)

    if not np.isclose(frac_, 0.5) or int_ != 0:
        return r"%g"%x

    intpart = "" if int_ == 0 else "%d"%int_

    fracpart = r"\nicefrac{1}{2}"

    return r"$" + intpart + fracpart + r"$"


au2fs = 1.0/241.8884326505
au2GV_m = 5.14220652*100

colors = ["#1f77b4",
    "#ff7f0e",
    "#ffbb78",
    "#2ca02c",
    "#98df8a",
    "#d62728",
    "#ff9896",
    "#9467bd",
    "#c5b0d5",
    "#8c564b",
    "#c49c94",
    "#e377c2",
    "#f7b6d2",
    "#7f7f7f",
    "#c7c7c7",
    "#bcbd22",
    "#dbdb8d",
    "#17becf",
    "#9edae5"]

def _load_data(path, include_states):

    # Load the laser
    try:
        laser = np.loadtxt(path + "/simulation/laser")
    except:
        raise RuntimeError("Laser file does not exist at location: "+
                           path+"/simulation/laser")

    # Load populations
    try:
        data = np.load(path+"/populations_td.npz")
        times = data['times']
        populations_in_time = data['populations'].tolist()
        
        # Check that all requested states have been computed
        for state in include_states:
            if state not in populations_in_time:
                raise RuntimeWarning(("Not all requested states "
                                      "[e.g., (%d,%d)] "
                                      "have been "
                                      "precomputed. We'll need to precompute "
                                      "them all")%state)
    except Exception as e:
        times, populations_in_time = calculate_populations(path, include_states)

    # Convert everything to fs and GV/m
    time = times*au2fs
    laser[:,0] *= au2fs
    laser[:,1] *= au2GV_m

    return laser, time, populations_in_time



def _setup_figure(nmin, nmax, lmin, lmax):
    lmax = np.min([lmax, nmax-1])
    # Setup the figure
    fig = plt.figure(figsize=(3.41666666,9))
    fig.subplots_adjust(hspace=0, left=0.17, right=0.85, wspace=0.02, bottom=0.07,
                   top=0.9)

    gs_gen = gridspec.GridSpec(1,2, width_ratios = [9,1])
    
    num_lvalues = int(lmax-lmin+1)
    num_nvalues = int(nmax-nmin+1)
    num_subplots = 2 + num_lvalues #laser + spectrogram + 1 subplot per angular qn

    gs = gridspec.GridSpecFromSubplotSpec(num_subplots,1, subplot_spec=gs_gen[0])

    gs0 = gridspec.GridSpecFromSubplotSpec(num_subplots, 1, subplot_spec=gs_gen[1])

    # Plot the color guide next to populations graphs
    ax = plt.subplot(gs0[2:])
    ax.set_title(r'$n$')
    ax.set_aspect(1)
    ax.set_xlim(0,0.2)
    ax.set_ylim(0,1)
    ax.axis('off')
    

    def add_rect(n, ax):
        rect = Rectangle((0.0, 1-(n-nmin+1)/num_nvalues), 
                         width=0.1, 
                         height=1/num_nvalues, 
                         color=colors[n-1])
        ax.add_patch(rect)
        ax.text(0.12, 1-(n-nmin+1)/num_nvalues+0.5/num_nvalues, r'$%d$'%n)

    for i in range(num_nvalues):
        add_rect(i+1, ax)

    # laser axis
    ax_laser = plt.subplot(gs[0])

    ax_laser.xaxis.set_label_position('top')
    ax_laser.xaxis.set_ticks_position('top')
    ax_laser.set_xlabel(r'time (fs)')
    ax_laser.set_ylabel(r'\begin{center}electric field\\(GV/m)\end{center}')
    ax_laser.yaxis.set_label_coords(-0.11, 0.5)

    ax_laser.yaxis.set_major_formatter(FuncFormatter(nice_formatter))

    # Setup spectrogram axes
    ax_specto = plt.subplot(gs[1], sharex=ax_laser)
    ax_specto.set_ylim((0,5))
    ax_specto.set_ylabel(r'energy (eV)')
    ax_specto.yaxis.set_major_formatter(FuncFormatter(nice_formatter))
    cax_specto = plt.subplot(gs0[1])
    
    spectrogram_axes_pair = (ax_specto, cax_specto)


    # Create populations axes
    populations_ax_list = []
    for l in range(int(lmin), int(lmax+1)):
        ax = plt.subplot(gs[2+l-lmin], sharex = ax_laser)
        if l== lmax:
            ax.set_xlabel(r'time (fs)')
        ax.yaxis.set_major_formatter(FuncFormatter(nice_formatter))
        ax.xaxis.set_major_formatter(FuncFormatter(nice_formatter))
        ax.text(0.05, 0.73, r'$l=%d$'%l, transform=ax.transAxes)
        if l == np.mean([lmin, lmax], dtype=int):
            ax.set_ylabel('population')
        
        populations_ax_list.append(ax)

    return fig, ax_laser, spectrogram_axes_pair, populations_ax_list

def simulation_overview():
    parser = argparse.ArgumentParser(description='Overview of a single-shot simulation')

    parser.add_argument('path', nargs="?", default=os.getcwd(), help='Path to the root of the simulation')
    parser.add_argument('--l-max', type=int, default=np.inf,
                        help='Maximum angular quantum number to visualize')
    parser.add_argument('--n-min', type=int, default=1,
                        help='Minimum principal quantum number to visualize')
    parser.add_argument('--n-max', type=int, default=3,
                        help='Maximum principal quantum number to visualize')
    parser.add_argument('--stft-window-length', type=float, default=1,
                        help='Window length (in fs) for the spectrogram')

    parser.add_argument('--compute-final-populations', type=str, default="",
                        help=('List of states whose final population '
                              'is calculated'), nargs='+')
    args = parser.parse_args()

    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['text.latex.preamble'] =\
    r'\usepackage{nicefrac}\usepackage{mathtools}\usepackage{amssymb}\usepackage{newpxmath}\usepackage{newpxtext}'

    # Which states to include in the figure
    states = _list_hydrogenic_states(args.n_min, args.n_max, 0,
                                     args.l_max)
  
    # Check if requested computation of final overlap of states not already in
    # the list of states for which we plot the populations
    requested_final_population_states = [ (int(s.split(',')[0]),
                                               int(s.split(',')[1])) for s in
                                         args.compute_final_populations ]

    add_these = [ state for state in requested_final_population_states if state not in
     states]


    laser, times, populations = _load_data(args.path, states)

    fig, ax_laser, specto_axes, populations_axes = _setup_figure(args.n_min,
                                                                 args.n_max,
                                                                 0,
                                                                 args.l_max)

    # Plot laser
    ax_laser.plot(laser[:,0], laser[:,1])

    # Plot spectrogram
    samplerate = 1/(laser[1,0]-laser[0,0])
    window_width = args.stft_window_length*samplerate
    f, t, Z = stft(x = laser[:,1], fs = samplerate, nperseg = window_width,
                   noverlap = window_width/4*3,
                   window=('gaussian',window_width*0.5),
                   boundary ='zeros')
    f *= 0.658212
    T, F = np.meshgrid(t, f)
    im = specto_axes[0].pcolormesh(T, F, np.abs(Z)**2, cmap='magma_r')
    
    cbar = plt.colorbar(im, cax=specto_axes[1])
    cbar.set_ticks(cbar.get_clim())
    cbar.set_ticklabels([r'low', r'high'])
    cbar.set_label(r'intensity', labelpad=-15)

    # Plot populations

    for n,l in states:
        population = populations[n,l]
        populations_axes[l].plot(times, population, color=colors[n-1], lw=0.5)

    # Compute total final population of a few requested states:
    target_popul = 0.0
    for n,l in requested_final_population_states:
        target_popul += populations[n,l][-1]

    if len(requested_final_population_states) != 0:
        fig.suptitle('Target population: %g' % target_popul, fontsize=10)

    plt.show()

if __name__ == '__main__':
    simulation_overview()
