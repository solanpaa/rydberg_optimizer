#!/usr/bin/env python3
from rydberg_optimizer.common import *
from rydberg_optimizer.common.parser import _list_hydrogenic_states 
from rydberg_optimizer.states.rydberg_states import RydbergStates
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def _plot_overlaps_core(ax, S, color='m', edgecolor=None):
    """Give ax and the overlaps dictionary"""
    lmax = max(S.keys(),key= lambda s: s[1])[1]
    nmax = max(S.keys(), key= lambda s: s[0] )[0]
    lmin = min(S.keys(),key= lambda s: s[1])[1]
    nmin = min(S.keys(), key= lambda s: s[0] )[0]
    Nr = np.arange(nmin, nmax+1, dtype=int)
    Lr = np.arange(lmin, lmax+1, dtype=int)
    N, L = np.meshgrid(Nr, Lr)
    overlaps = np.zeros((len(Nr), len(Lr)))
    for key, value in S.items():
        n = key[0]
        l = key[1]
        overlaps[n-nmin,l-lmin] = value

    elems = overlaps.size  

    Nflat = N.flatten('C')
    Lflat = L.flatten('C')
    Sflat = (overlaps.T).flatten('C')
    Smasked = np.ma.masked_array(Sflat, Lflat < Nflat)

    barplot = ax.bar3d(Nflat-0.25, Lflat-0.25,
             np.zeros_like(N.flatten('C')), 0.5*np.ones(elems),
             0.5*np.ones(elems), Smasked, alpha=0.75, color=color,
                       edgecolor=edgecolor)
    ax.set_xlim(0.5,nmax+0.5)
    ax.set_xticks(Nr)
    ax.set_xticklabels(Nr)
    ax.set_ylim(-0.5, lmax+0.5)
    ax.set_yticks(Lr)
    ax.set_yticklabels(Lr)

    ax.set_xlabel(r'$n$')
    ax.set_ylabel(r'$l$')
    ax.set_zlabel(r'$\vert\langle n,l,m=0\vert\psi\rangle\vert^2$')
    return barplot
 
def plot_overlaps():
    import argparse
    parser = argparse.ArgumentParser(description="""Plots the final state
                                     populations""")
    parser.add_argument('--path', default='simulation/real_time-wf.dat')
    args = parser.parse_args()
    path = args.path

    # Parse the input file
    sconfig, laser, oct_config, _, _, _ =\
            get_config('/'.join(path.split('/')[:-1]))
    rs = RydbergStates()
    nmax = max( oct_config['states'], key= lambda s: s[0] )[0]
    states = _list_hydrogenic_states(1, nmax)
    S, total_overlap = rs.calculate_overlaps(sconfig, states, path)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    _plot_overlaps_core(ax, S)

    plt.show()

if __name__ == '__main__':
    plot_overlaps()
