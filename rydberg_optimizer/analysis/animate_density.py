#!/usr/bin/env python3

from rydberg_optimizer.common import *
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import os
from rydberg_optimizer.analysis.wf_tools import read_qprop_wf_xy_cut
from glob import glob
from matplotlib.animation import FuncAnimation

def run_animate():
    simulation_config, _, _, _, _, _ = get_config()

    #A couple checks for problems
    files = os.listdir("./")
    if not "simulation" in files:
        print("Data not found")
        exit(1)
    datafiles = np.array(glob("simulation/real_time-wf.dat_*"))
    timeidx = [ int(filename.split('_')[2]) for filename in datafiles ]
    datafiles = datafiles[np.argsort(timeidx)]
 
    if not "simulation/real_time-wf.dat_0" in datafiles:
        print("Data not found")
        exit(1)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(r'$x$-coordinate (a.u.)')
    ax.set_ylabel(r'$z$-coordinate (a.u.)')
    normalizer = LogNorm()
    datafile = datafiles[0]
    X, Y, Z = read_qprop_wf_xy_cut(datafile)
    Z = np.abs(Z)**2
    normalizer = LogNorm(vmin=Z.min(), vmax=Z.max())
    p = ax.pcolormesh(X, Y, Z, cmap = 'viridis', norm=normalizer)
    ax.set_aspect(1)
    fig.tight_layout(pad=1)
    def init():
        datafile = datafiles[0]
        X, Y, Z = read_qprop_wf_xy_cut(datafile)
        Z = np.abs(Z)**2
        normalizer = LogNorm(vmin=Z.min(), vmax=Z.max())
        p = ax.pcolormesh(X, Y, Z, cmap = 'viridis', norm=normalizer)
        ax.set_aspect(1)
        fig.tight_layout(pad=1)
        return p,

    def update(datafile):
        _, _, Z = read_qprop_wf_xy_cut(datafile)
        Z = np.abs(Z)**2
        p.set_array(Z[:-1, :-1].ravel())
        return p,
        
    ani = FuncAnimation(fig, update, frames = datafiles, init_func = init,
                        blit=True, interval=1, save_count = 10, repeat=False)
    plt.show()
    ani.save("density.mp4", writer="mencoder")

if __name__ == '__main__':
    run_animate()
