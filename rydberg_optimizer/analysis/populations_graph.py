#!/usr/bin/env python3
from rydberg_optimizer.common import *
from rydberg_optimizer.states.rydberg_states import RydbergStates
import numpy as np
import os
from glob import glob
import argparse
import progressbar
import sys

def calculate_populations(path, states):
    sconfig, laser, _, _, _, _ = get_config(path)
     
    print("Calculating time-dependent populations of field-free states (n,l):\n    ", end='')
    for state in states:
        print("%d,%d "%state, end='')
    print()
    rs = RydbergStates()

    #A couple checks for problems
    files = os.listdir(path)
    if not "simulation" in files:
        raise RuntimeError("Simulation datadir not found: "+ path+"/simulation")

    # Load and sort datafiles
    datafiles = np.array(glob(path+"/simulation/real_time-wf.dat_*"))
    timeidx = np.array( [int(filename.split('/')[-1].split('_')[2]) for filename in datafiles
                        ])
    times = sconfig['delta-t']*timeidx
    datafiles = datafiles[np.argsort(timeidx)]

    times = np.sort(times)

    # Checks
    if not path+"/simulation/real_time-wf.dat_0" in datafiles:
        raise RuntimeError("Computed wavefunctions not found: "+
                           path+"/simulation/real_time-wf.dat_0")
    
    # Calculate the initial populations
    S, total_overlap = rs.calculate_overlaps(sconfig,
                                             states,
                                             datafiles[0])
    
    # Allocate buffer
    populations_in_time = {}
    for n,l in states:
        populations_in_time[n,l]=np.zeros(len(times))

    # Calculate populations at each instance of time
    bar = progressbar.ProgressBar(redirect_stdout=True, max_value =
                                  len(datafiles))
    for i, datafile in bar(enumerate(datafiles)):

        S, _ = rs.calculate_overlaps(sconfig,
                                                 states,
                                                 path=datafile)
        for n,l in states:
            populations_in_time[n,l][i] = S[n,l]

    # Save populations to file
    np.savez("populations_td.npz", times=times, populations = populations_in_time)
 
    return times, populations_in_time

def plot_populations():

    parser = argparse.ArgumentParser(description="""This script visualizes the
                                     populations of field-free
                                     energy-eigenstates during a
                                     time-propagation of a single
                                     simulation.""")
    parser.add_argument("path", nargs='?',
                        default=os.getcwd(),
                        help="Path to the simulation root")

    grp = parser.add_mutually_exclusive_group()
    grp.add_argument("--exclude", "-e", nargs='+', help='List of states to exclude. Space separated list of n,l.')
    grp.add_argument("--include", "-i", nargs='+', help="""States to include.
                     Space separated list of n,l. Defaults to target states
                     from the oct simulation.""")
    parser.add_argument('--save','-s', action='store_true', default=False,
                        help="Saves plots and data instead of interactively"+
                        " visualizing it")


    args = parser.parse_args()
    # Parse the input file
    _, _, oct_config, _, _, _ = get_config(args.path)
    if args.save:
        import matplotlib
        matplotlib.use('Agg')

    import matplotlib.pyplot as plt
    
    if args.include is None:

        include_states = oct_config['states']
        
        exclude_str = args.exclude
        exclude_states=[]
        if exclude_str is not None:
            exclude_states = [ (int(s.split(',')[0]), int(s.split(',')[1])) for s in
                              exclude_str ]
            for key in exclude_states:
                include_states.remove(key)
    else:
        include_str = args.include
        include_states = [ (int(s.split(',')[0]), int(s.split(',')[1])) for s in
                              include_str ]
    try:
        data = np.load(args.path+"/populations_td.npz")
        times = data['times']
        populations_in_time = data['populations'].tolist()
        
        # Check that all requested states have been computed
        for state in include_states:
            if state not in populations_in_time:
                raise RuntimeWarning(("Not all requested states "
                                      "[e.g., (%d,%d)] "
                                      "have been "
                                      "precomputed. We'll need to precompute "
                                      "them all")%state)
    except Exception as e:
        print(e)
        times, populations_in_time = calculate_populations(args.path, include_states)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for n,l in include_states:
        ax.plot(times, populations_in_time[n,l], label=r'(n,l)=(%d,%d)'%(n,l))

    ax.set_xlabel(r'time (a.u.)')
    ax.set_ylabel(r'population')
    ax.legend()
    
    fig.tight_layout()
    if args.save:
        plt.savefig('populations.pdf')
    else:
        plt.show()

if __name__ == '__main__':
    try:
        plot_populations()
    except Exception as e:
        print("%s: %s" % (type(e).__name__, e))
