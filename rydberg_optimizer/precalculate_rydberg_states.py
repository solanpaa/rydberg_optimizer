#!/usr/bin/env python3 

from rydberg_optimizer.states.rydberg_states import RydbergStates
from rydberg_optimizer.common.parser import get_config
import logging
import os, sys

def precalculate_states_main():
    name, extension = os.path.splitext(os.path.basename(sys.argv[0]))
    simulation_config, laser, oct_config, constraints, variable_limits, initial_value = get_config()
    # Set up logging
    if simulation_config['log_to_file']:
        logging.basicConfig(filename=name + ".log",
                            level=logging.DEBUG, format='%(message)s')
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(message)s')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
    else:
        logging.basicConfig(level=logging.DEBUG, format='%(message)s')


    rs = RydbergStates()

    print("Calculating states...")
    rs.calculate_rydberg_states(simulation_config, oct_config['states'])
    print("... done")

if __name__ == '__main__':
    precalculate_states_main()
