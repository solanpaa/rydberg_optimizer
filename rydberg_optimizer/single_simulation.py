#!/usr/bin/env python3
from rydberg_optimizer.common.utils import *
from rydberg_optimizer.common.constraints import *
from rydberg_optimizer.common.common_setup import setup_constraints, get_allowed_initial_quess
from rydberg_optimizer.qprop import *
from rydberg_optimizer.states.rydberg_states import RydbergStates
from numpy import sqrt, power, linspace, savetxt, array
import random
import shutil
import nlopt
import os
import errno
import logging

def single_main():
    # Get the name and extension of this program
    name, extension = os.path.splitext(os.path.basename(sys.argv[0]))

    # Parse the input file
    simulation_config, laser, oct_config, constraints, variable_limits, initial_value = get_config()

    # Set up logging
    if simulation_config['log_to_file']:
        logging.basicConfig(filename=name + ".log",
                            level=logging.DEBUG, format='%(message)s')
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(message)s')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
    else:
        logging.basicConfig(level=logging.DEBUG, format='%(message)s')

    logger = logging.getLogger('')
    
    mkdir_p("simulation")

    result_container = {}
    constr_funs = setup_constraints(
        constraints, laser, variable_limits)


    if (len(initial_value) == 0):
        logger.info("Running a single simulation with a fully specified pulse configuration.")
    else:
        logger.warning("Running a single simulation with a partially specified pulse configuration.")
        initial_value = get_allowed_initial_quess(
            laser, variable_limits, constr_funs)

    laser.print_parameters()    
    # Calculate GS
    simulation_config['vecpot-file'] = 'vecpot'
    qprop_write_input_file(simulation_config)

    rs = RydbergStates()
    qprop_prepare_initial_state(simulation_config, rs)
    # Do a TD simulation
    T0 = laser.beginning_of_pulse()
    Tmax = laser.end_of_pulse()
    logger.info("Simulation time: %.1f - %.1f" % (T0, Tmax))

    # Make time to start at t=0
    TIME = np.linspace(T0, Tmax, 10000) - T0

    # Precalculate the fields
    def Elaser(t): return laser.electric_field(T0 + t)

    c_io.savetxt('simulation/laser',
                 np.vstack((TIME - TIME[0], Elaser(TIME))).T, header=laser.get_parameters_str())

    vecpot = -laser(T0 + TIME)

    c_io.savetxt('simulation/vecpot',
                 np.vstack((TIME - TIME[0], vecpot)).T, header=None)


    # Simulate TD run
    qprop_calculate_td()

if __name__ == '__main__':
    single_main()
