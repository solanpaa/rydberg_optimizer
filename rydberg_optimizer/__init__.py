"""
This package is a wrapper interfacing the nlopt optimization library and 
Qprop atomic simulation package for quantum optimal control simulations
of ultrafast atomic processes.
"""

__author__ = "Janne Solanpää, Tuomas Tinus"

__author_email__ = "janne@solanpaa.fi"

__license__ = "GPLv3"

__version__ = "0.3.12"
