# compatible with nlopt's opt-class
import numpy as np
import nlopt
# See Esteban's paper: https://arxiv.org/pdf/1607.02309.pdf


class nlopt_spa:

    # Initialization of the optimizer
    def __init__(self, dim, **kwargs):
        self.curdim = 0
        self.params = {
            'dim': dim,
            'max_time': None,
            'max_eval': 300,
            'xtol_abs': None,
            'xtol_rel': 1e-5,
            'ftol_abs': None,
            'ftol_rel': None,
            'min_objective': None,
            'lower_bounds': None,
            'upper_bounds': None,
            'last_result': None,
            'last_value': None,
            'maximize': False,
            'normalized_fidelity': True,
            'local_optimizer_generator': None,
            'nbr_new_parameters': 2,
            'nbr_paramset_evals': 50,
            'constraints': None,
            'verbose': False
        }
        for key, val in kwargs.items():
            if key in self.params:
                self.params[key] = val
            else:
                raise KeyError(
                    "Provided argument not applicable to the spa optimizer routine.")

    def set_local_optimizer(self, *args):
        self.params['local_optimizer_generator'] = args[0]

    def _increase_nbr_of_variables(self):
        dim = self.params['dim']
        if self.curdim == dim:
            return

        if isinstance(self.params['nbr_new_parameters'], int):
            self.curdim = int(
                np.min([dim, self.curdim + self.params['nbr_new_parameters']]))
        else:
            if len(self.params['nbr_new_parameters']) > 1:
                nextaddition = self.params['nbr_new_parameters'].pop(0)
            else:
                nextaddition = self.params['nbr_new_parameters'][0]

            self.curdim = int(np.min([dim, self.curdim + nextaddition]))

    def _set_local_optimizer(self, opt, x0):
        opt.set_maxeval(self.params['nbr_paramset_evals'])

        def target(x_, grad_):
            x__ = np.concatenate((x_, x0[len(x_):]))
            return self.params['min_objective'](x__, grad_)
        opt.set_min_objective(target)
        opt.set_lower_bounds(self.params['lower_bounds'][:self.curdim])
        opt.set_upper_bounds(self.params['upper_bounds'][:self.curdim])

    # Runs the optimization routine
    # Takes one argument which is the initial quess
    # (a numpy array)
    def optimize(self, *args):

        x0 = args[0]
        target = self.params['min_objective']
        fval0 = target(x0)
        dim = self.params['dim']

        # Set first optimization dimension
        self._increase_nbr_of_variables()

        n = 0
        while True:

            if self.params['verbose']:
                print("Iteration %d: DIM %d, f %f, x:" %
                      (n, self.curdim, fval0), x0)

            opt = self.params['local_optimizer_generator'](self.curdim)
            self._set_local_optimizer(opt, x0)

            n += 1

            # Runs nbr_paramset_evals number of iterations
            x = opt.optimize(x0[:self.curdim])
            x = np.concatenate([x, x0[self.curdim:]])

            fval = opt.last_optimum_value()

            converged = True
            if self.params['ftol_abs']:
                ftol_abs = np.fabs(fval - fval0)

                if self.params['verbose']:
                    print("Converged since ftol_abs = %f" % ftol_avs)
                converged = converged and (ftol_abs < self.params['ftol_abs'])
            if self.params['ftol_rel']:
                ftol_rel = np.fabs(fval - fval0) / np.fabs(fval)

                if self.params['verbose']:
                    print("Converged since ftol_rel = %f" % ftol_rel)
                converged = converged and (ftol_rel < self.params['ftol_rel'])
            if self.params['xtol_abs']:
                xtol_abs = np.linalg.norm(x - x0)
                convthis = xtol_abs < self.params['xtol_abs']

                if self.params['verbose'] and convthis:
                    print("Converged since xtol_abs = %f" % xtol_abs)
                converged = converged and convthis
            if self.params['xtol_rel']:
                xtol_rel = np.linalg.norm(x - x0) / np.linalg.norm(x)
                convthis = xtol_rel < self.params['xtol_rel']
                converged = converged and convthis
                if self.params['verbose'] and convthis:
                    print("Converged since xtol_rel = %f" % xtol_rel)

            if self.params['max_eval']:
                nbr_evals = n * self.params['nbr_paramset_evals']
                convthis = (nbr_evals >= self.params['max_eval'])
                if self.params['verbose'] and convthis:
                    print("Converged since nbr_evals = %d" % nbr_evals)
                converged = converged or convthis

            if (converged):
                if dim == self.curdim:
                    break
                else:
                    self._increase_nbr_of_variables()

            x0 = x
            fval0 = fval

        self.params['last_result'] = x
        self.params['last_value'] = fval

        return self.params['last_result']

    # Returns the argument that gives the optimal value
    def last_optimize_result(self):
        return self.params['last_result']

    # Returns the optimal value
    def last_optimum_value(self):
        return self.params['last_value']

    # Returns the algorithm 'name'.
    def get_algorithm(self):
        return "sep"

    # Returns the algorithm name
    def get_algorithm_name(self):
        return "Sequential parameter update"

    # Returns the dimension of the search space
    def get_dimension(self):
        return self.params['dim']

    # Set objective for minimization
    def set_min_objective(self, *args):
        def wrapper(x, grad=np.array([])):
            if len(x.shape) != 1:
                return np.array([args[0](elem, grad) for elem in x])
            else:
                return args[0](x, grad)
        self.params['min_objective'] = wrapper

    # Set objective for maximization
    def set_max_objective(self, *args):
        self.set_min_objective(lambda x, grad: -args[0](x, grad))
        self.params['maximize'] = True

    # Gets lower bounds of the optimization hypercube
    def get_lower_bounds(self, *args):
        return self.params['lower_bounds']

    # Sets lower bounds of the optimization hypercube
    # Argument: float or numpy array
    # If float, all optimization parameters have same lower bound
    # If numpy array, one lower bound per optimization parameter
    def set_lower_bounds(self, *args):
        self.params['lower_bounds'] = args[0]

    # Gets upper bounds of the optimization hypercube
    def get_upper_bounds(self, *args):
        return self.params['upper_bounds']

    # Sets upper bounds of the optimization hypercube
    # Argument: float or numpy array
    # If float, all optimization parameters have same upper bound
    # If numpy array, one upper bound per optimization parameter
    def set_upper_bounds(self, *args):
        self.params['upper_bounds'] = args[0]

    # Gets the stopping criteria for absolute change in the argument value
    def get_xtol_abs(self, *args):
        return self.params['xtol_abs']

    # Sets the stopping criteria for absolute change in the argument value
    # Argument: float or numpy array
    # If float, all optimization parameters must obey same tolerance
    # If numpy array, one tolerance per optimization parameter
    def set_xtol_abs(self, *args):
        self.params['xtol_abs'] = args[0]

    # Gets the maximum number of function evaluations
    def get_maxeval(self):
        return self.params['max_eval']

    # Sets the maximum number of function evaluations
    # Argument: integer
    def set_maxeval(self, *args):
        self.params['max_eval'] = args[0]

    # Gets the maximum simulation time in seconds
    def get_maxtime(self):
        return self.params['max_time']

    # Sets the maximum simulation time in seconds
    # Argument: float
    def set_maxtime(self, *args):
        self.params['max_time'] = args[0]

    def get_stopval(self):
        return self.params['fval_stop']

    def set_stopval(self, *args):
        self.params['fval_stop'] = args[0]

    def get_ftol_rel(self):
        return self.params['ftol_rel']

    def set_ftol_rel(self, *args):
        self.params['ftol_rel'] = args[0]

    def get_ftol_abs(self):
        return self.params['ftol_abs']

    def set_ftol_abs(self, *args):
        self.params['ftol_abs'] = args[0]

    def get_xtol_rel(self):
        return self.params['xtol_rel']

    def set_xtol_rel(self, *args):
        self.params['xtol_rel'] = args[0]

    # The following are implemented only for the sake of
    # full interface compatibility with nlopt
    def add_inequality_constraint(self, *args):
        NotImplementedError

    # Adds a scalar-valued equality constraint: c(x) == 0
    # Argument 1: the function c
    def add_equality_constraint(self, *args):
        NotImplementedError

    def set_initial_step(self, *args):
        raise NotImplementedError

    def set_default_initial_step(self, *args):
        raise NotImplementedError

    def get_initial_step(self, *args):
        raise NotImplementedError

    def get_initial_step_(self, *args):
        raise NotImplementedError

    def force_stop(self):
        raise NotImplementedError

    def get_force_stop(self):
        raise NotImplementedError

    def set_force_stop(self, *args):
        raise NotImplementedError

    def get_population(self):
        raise NotImplementedError

    def set_population(self, *args):
        raise NotImplementedError

    def get_vector_storage(self):
        raise NotImplementedError

    def set_vector_storage(self, *args):
        raise NotImplementedError

    def add_inequality_mconstraint(self, *args):
        raise NotImplementedError

    def add_equality_mconstraint(self, *args):
        raise NotImplementedError

    def remove_inequality_constraints(self):
        raise NotImplementedError

    def remove_equality_constraints(self):
        raise NotImplementedError


if __name__ == '__main__':
    def lopt_generator(dim):
        _opt = nlopt.opt(nlopt.LN_COBYLA, dim)
        _opt.set_xtol_rel(0.01)
        _opt.set_ftol_rel(0.01)
        return _opt

    print("Testing")

    def fun(x, grad): return np.sin(
        x[0]**2) * np.tan(np.fabs(x[1])) * np.exp(np.sqrt(np.fabs(x[2]))) * np.sqrt(np.fabs(x[3]))
    opt = nlopt_spa(4, local_optimizer_generator=lopt_generator,
                    nbr_new_parameters=1, verbose=True, max_eval=2000, xtol_rel=1e-18)
    opt.set_lower_bounds([-2, -2, -2, -2])
    opt.set_upper_bounds([2, 2, 2, 2])
    opt.set_min_objective(fun)
    opt.set_maxeval(2000000)
    xopt = opt.optimize(np.array([0.05, 0.25, -0.1, 0.5]))
    print(xopt)
