#!/usr/bin/env python3

import numpy as np
from rydberg_optimizer.common.parameter import *
import logging

def _conv_type(x):
    if isinstance(x, Parameter):
        return x
    else:
        return Parameter(x, True)

class SingleColorLaserPulse:

    """
    This class represents a single-color laser pulse. Internally, it's
    implemented as vector potential.
    """

    def __init__(self, A, w, k, tau, phi, sigma):
        A=_conv_type(A)
        w=_conv_type(w)
        k=_conv_type(k)
        tau=_conv_type(tau)
        phi=_conv_type(phi)
        sigma=_conv_type(sigma)

        self.params = np.array([A, w, k, tau, phi, sigma])
        self.A = self.params[0]
        self.w = self.params[1]
        self.k = self.params[2]
        self.tau = self.params[3]
        self.phi = self.params[4]
        self.sigma = self.params[5]
        self.logger = logging.getLogger('')
        self.num_variables = 0
        for param in self.params:
            if(not param.is_const):
                self.num_variables += 1

    def T(self):
        return 2*self.sigma.val

    def get_copy(self):
        pulse = SingleColorLaserPulse(self.A, self.w, self.k, self.tau, self.phi, self.sigma)
        return pulse

    def set_vars(self, varlist):
        assert len(varlist) == self.num_variables, "Invalid number of parameters for LaserPulse"
        n=0
        for i in range(len(self.params)):
            if(not self.params[i].is_const):
                self.params[i].val = varlist[n]
                n=n+1
        assert n==len(varlist), "Invalid number of parameters for LaserPulse"
        self.A = self.params[0]
        self.w = self.params[1]
        self.k = self.params[2]
        self.tau = self.params[3]
        self.phi = self.params[4]
        self.sigma = self.params[5]

    def get_vars_str(self):
        return "    Pulse: A=%.6f, w=%.6f, k=%.6f, τ=%.6f, φ=%.6f, σ=%.6f" % (self.A.val, self.w.val, self.k.val, self.tau.val, self.phi.val, self.sigma.val)

    def print_vars(self):
        self.logger.info( "    Pulse: A=%.2f, w=%.4f, k=%.2f, τ=%.2f, φ=%.2f, σ=%.2f" % (self.A.val, self.w.val, self.k.val, self.tau.val, self.phi.val, self.sigma.val) )

    def carrier(self, t):
        return np.cos((self.w.val+self.k.val*(t-self.tau.val))*(t-self.tau.val)+self.phi.val)

    def envelope(self, t):
        fun = np.vectorize(self._envelope_core)
        return fun(t)

    def _envelope_core(self, t):
        if np.fabs(t - self.tau.val) < self.T():
            return np.exp(-np.log(2.0)*((t-self.tau.val)/self.sigma.val)**2/ (1-(t-self.tau.val)**2/self.T()**2))
        else:
            return 0.0

    def partial_t_carrier(self, t):
        return -(2*self.k.val*(t-self.tau.val)+self.w.val)*np.sin((self.w.val+self.k.val*(t-self.tau.val))*(t-self.tau.val)+self.phi.val)

    
    def partial_t_envelope(self, t):
        fun = np.vectorize(self._partial_t_envelope_core)
        return fun(t)

    def _partial_t_envelope_core(self, t):
        if np.fabs(t-self.tau.val)<self.T():
            return -2*np.log(2)*self.envelope(t) * (t-self.tau.val)/(self.sigma.val**2 * (1-((t-self.tau.val)/self.T())**2))*(1+((t-self.tau.val)/self.T())**2 * 1.0/(1-((t-self.tau.val)/self.T())**2))
        else:
            return 0.0
    def __call__(self, t):
        return self.A.val/self.w.val*self.envelope(t)*self.carrier(t)

    def beginning_of_pulse(self):
        return self.tau.val-2*self.sigma.val

    def end_of_pulse(self):
        return self.tau.val+2*self.sigma.val

    def get_number_of_variables(self):
        return self.num_variables

    def partial_A(self, t):
        return self.carrier(t)*self.envelope(t)

    def electric_field(self, t):
        return -self.A.val/self.w.val*(self.partial_t_carrier(t)*self.envelope(t)+self.partial_t_envelope(t)*self.carrier(t))

    def is_A_const(self):
        return self.A.is_const

    def is_w_const(self):
        return self.w.is_const

    def is_k_const(self):
        return self.k.is_const

    def is_tau_const(self):
        return self.tau.is_const

    def is_phi_const(self):
        return self.phi.is_const

    def is_sigma_const(self):
        return self.sigma.is_const

    def partial_w(self, t):
        return None

    def partial_A(self, t):
        return None

    def partial_k(self, t):
        return None

    def partial_tau(self, t):
        return None

    def partial_phi(self, t):
        return None

    def partial_sigma(self, t):
        return None
