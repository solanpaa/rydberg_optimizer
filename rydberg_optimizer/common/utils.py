import os, errno
def mkdir_p(path):
    """
    By tzot in http://stackoverflow.com/a/600612/738107.
    """
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise exc


def str_to_bool(s):
    """Converts string to bool"""
    s = s.lower().strip()
    if s in ['true', 'yes', '1', 'aye', 'yeah']:
        return True
    elif s in ['false', 'no', '0', 'nay', 'nope']:
        return False
    else:
        raise ValueError("Invalid value to str_to_bool: " + s)


