#!/usr/bin/env python3

import os
import sys
import numpy as np

# Save a numpy array and header with utf-8 encoded header
# to a file
#
# Input:
#   filename    name of the file
#   data        numpy array to be saved
#   header      header as a text string
#   comment     comment symbol for header
#


def savetxt(filename, data, header, comment="#"):
    f = open(filename, "w")
    if header != None:
        headerstr = ""
        for line in header.split('\n'):
            headerstr += comment + line + "\n"
        f.write(headerstr)
        f.close()
    f = open(filename, "ab")
    np.savetxt(f, data)
    f.close()
