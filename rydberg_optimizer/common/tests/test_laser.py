#!/usr/bin/env python3

import unittest
import numpy as np
from nose.tools import assert_equals
from rydberg_optimizer.common.laser import *

class TestLaser(unittest.TestCase):
    def test_threecolor(self):
        pulse = SingleColorLaserPulse(Parameter(0.1, False), Parameter(0.0569, False), Parameter(
            0.0, True), Parameter(700.0, True), Parameter(0.0, True), Parameter(300.0, True))
        pulse2 = SingleColorLaserPulse(Parameter(0.1, False), Parameter(0.11, True), Parameter(
            0.0, False), Parameter(700.0, False), Parameter(0.0, True), Parameter(300.0, True))
        pulse3 = SingleColorLaserPulse(Parameter(0.1, True), Parameter(0.11, True), Parameter(
            0.0, True), Parameter(700.0, False), Parameter(0.0, False), Parameter(300.0, False))
        laser = Laser()
        laser.add_pulse(pulse)
        laser.add_pulse(pulse2)
        laser.add_pulse(pulse3)

        t = np.linspace(laser.beginning_of_pulse(),
                        laser.end_of_pulse(), 150000)
        assert np.allclose(laser.electric_field(t), pulse.electric_field(t) + pulse2.electric_field(t) +
                           pulse3.electric_field(t)), "Total Laser electric field differs from sum of single-color electric fields"
        assert np.allclose(laser.electric_field(t), -np.gradient(laser(
            t), t[1] - t[0]), atol=1e-7), "Electric field is not minus 1 times the time-derivative of vector potential"
