'''
Contains functions for global pulse constraints
'''
from rydberg_optimizer.common import *
import numpy as np

'''
Constraints the maximum duration of the pulse
Input:
    x     pulse parameters
    grad  do not use, but provide an empty array when called.
          Nlopt needs this parameter
    laser instance of the laser class
    maxt  the maximum duration that is desired
Returns:
   (actual_duration - max_duration)/max_duration
'''


def max_time_constraint(x, grad, laser, maxt):
    laser.set_vars(x)
    return (laser.duration_fwhm() - maxt) / maxt


'''
Constraints the peak electric field strenght of the pulse
Input:
    x     pulse parameters
    grad  do not use, but provide an empty array when called.
          Nlopt needs this parameter
    laser instance of the laser class
    maxfield  the maximum field that is desired
Returns:
   (actual_max_field - max_field)/max_field
'''


def max_field_constraint(x, grad, laser, maxfield):
    laser.set_vars(x)
    return (laser.max_field() - maxfield) / maxfield


'''
Constraints the fluence of the pulse
Input:
    x     pulse parameters
    grad  do not use, but provide an empty array when called.
          Nlopt needs this parameter
    laser instance of the laser class
    maxfluence the maximum fluence that is desired
Returns:
   (actual_max_fluence - max_fluence)/max_fluence
'''


def max_fluence_constraint(x, grad, laser, maxfluence):
    laser.set_vars(x)
    return (laser.calculate_fluence() - maxfluence) / maxfluence


'''
Constraints the ionization of the system
Input:
    x     pulse parameters
    grad  do not use, but provide an empty array when called.
          Nlopt needs this parameter
    laser instance of the laser class
    simulation_results   a container that is passed also to the
                         TDSE routine, which writes the amount of ionization there
    maxionization   maximum ionization allowed
Returns:
    (actual_ionizzation - maxionization) / maxionization
'''


def max_ionization_constraint(x, grad, laser, simulation_results, maxionization):
    laser.set_vars(x)
    return (simulation_results.ionization - maxionization) / maxionization
