#!/usr/bin/env python3
import configparser
import random
import re
import os
import errno
import nlopt
import numpy as np
from .laser import *
from .utils import str_to_bool
def _list_hydrogenic_states(nmin, nmax, lmin = 0, lmax = np.inf):
    """
    Returns a list of all possible (n,l) quantum number pairs
    for Hydrogenic states with nmin <= n <= nmax.
    """
    assert nmin >= 1, "n-quantum number must be >= 1"
    assert nmax >= nmin, "Please select states so that n_max >= nmin"
    statelist = []
    for n in np.arange(nmin, nmax+1, dtype=int):
        _lmin = np.max([0, lmin])
        _lmax = np.min([n, lmax+1])
        for l in np.arange(_lmin, _lmax, dtype=int):
            statelist.append((n,l))
    return statelist

def _parse_target_states(states_str):
    """
    Returns a list of (n,l) quantum numbers of the target states. Possible
    formats for the input string are:
        (1) NMIN:NMAX  which returns a list of all possible pairs (n,l) with
                       NMIN <= n <= NMAX
        (2) list of space-separated pairs, e.g., '(3,2) (2,1)'. The parenthesis
            are optional.
    """
    m = re.match(r"(?P<begin>\d+):(?P<end>\d+)", states_str) 
    if m:
        return _list_hydrogenic_states(int(m.group("begin")), int(m.group("end")))

    # match pairs
    p = re.compile("\(?\d+,\d+\)?")
    pairs = p.findall(states_str)
    assert len(pairs) == len(states_str.strip().split(" ")),\
            "Ill formatted target states"
    if len(pairs) != 0:
        statelist = []
        for pair in pairs:
            pair = pair.strip('()')
            pair = pair.split(',')
            n = int(pair[0])
            l = int(pair[1])
            assert l < n, "Provided states do not obey l = 0, ..., n-1"
            statelist.append( (n, l) )
        return statelist
    
    raise ValueError("Could not parse target states.")

def _parse_simulation_params(parser):
    """
    Parses `Simulation`-section of the input file.

    Parameters
    ----------
    parser : configparser.ConfigParser

    Returns
    -------
    config : dict
        Dictionary of the parameters
    """
    config = {}

    try:
        config['log_to_file'] = str_to_bool(
            parser.get("Simulation", "log_to_file"))
    except BaseException:
        config['log_to_file'] = True

    # qprop simulation parameters
    config['delta-r'] = parser.getfloat("Simulation", "delta-r")
    config['radial-grid-length'] = parser.getfloat(
        "Simulation", "radial-grid-length")
    config['ell-grid-size'] = parser.getfloat("Simulation", "ell-grid-size")
    config['save-interval'] = parser.getfloat("Simulation", "save-interval")
    config['atom-type'] = parser["Simulation"].get("atom-type", "H").lower()
    config['imag-width'] = parser.getfloat("Simulation", "imag-width")
    config['delta-t'] = parser.getfloat("Simulation", "delta-t")
    config['initial-state'] = _parse_target_states(parser.get("Simulation", "initial-state"))[0]

    # The following options are not used at the moment. Best just use these
    # default values. They are read from the input file in case we ever
    # decide to modify these values.
    config['ell-convergence-threshold'] = parser.getfloat(
        "Simulation", "ell-convergence-threshold", fallback=-1.0)
    config['ell-grid-max-size'] = parser.getfloat(
        "Simulation", "ell-grid-max-size", fallback=-1)
    config['check-ell-convergence'] = str_to_bool(
        parser.get("Simulation", "check-ell-convergence", fallback="nay"))
    if not config['check-ell-convergence']:
        config['ell-grid-max-size'] = config['ell-grid-size']
    config['qprop-dim'] = parser.getfloat("Simulation",
                                          "qprop-dim", fallback=34.0)
    config['initial-m'] = parser.getfloat("Simulation",
                                          "initial-m", fallback=0.0)
    config['initial-ell'] = parser.getfloat("Simulation", "initial-ell",
                                            fallback=0.0)

    config['r-tsurff'] = parser.getfloat("Simulation",
                                         "r-tsurff",
                                         fallback=config["radial-grid-length"])

    config['E-min-tsurff'] = parser.getfloat("Simulation", "E-min-tsurff",
                                             fallback=0.5)
    config['E-max-tsurff'] = parser.getfloat("Simulation", "E-max-tsurff",
                                             fallback=20.0)
    config['p-min-tsurff'] = np.sqrt(2 * config['E-min-tsurff'])
    config['k-max-tsurff'] = np.sqrt(2 * config['E-max-tsurff'])
    config['num-k-tsurff'] = parser.getfloat("Simulation", "num-k-tsurff",
                                             fallback=400.0)
    config['num-theta-tsurff'] = parser.getfloat(
        "Simulation", "num-theta-tsurff", fallback=30.0)
    config['num-phi-tsurff'] = parser.getfloat("Simulation", "num-phi-tsurff",
                                               fallback=10.0)
    config['delta-k-scheme'] = parser.getfloat("Simulation", "delta-k-scheme",
                                               fallback=2.0)
    config['cache-size-t'] = parser.getfloat("Simulation", "cache-size-t",
                                             fallback=10.0)
    config['expansion-scheme'] = parser.getfloat(
        "Simulation", "expansion-scheme", fallback=2.0)

    return config


def _parse_constraint_params(parser):
    """
    Parses `Constraints`-section of the input file.

    Parameters
    ----------
    parser : configparser.ConfigParser

    Returns
    -------
    config : dict
        Dictionary of the parameters
    """
    params = parser.items("Constraints")
    for key,_ in params:
        if key not in [
                       'maxduration',
                       'maxfield',
                       'maxfluence',
                       'maxionization']:
            raise Exception("No such option: Constraints::" + key[0])
    
    constraint_max_duration = parser["Constraints"].get("maxduration", False)

    constraint_max_fluence = parser["Constraints"].get("maxfluence", False)

    constraint_max_field = parser["Constraints"].get("maxfield", False)

    constraint_max_ionization = parser["Constraints"].get(
        "maxionization", False)

    constraints = {}
    if(constraint_max_duration):
        constraints['maxduration'] = constraint_max_duration
    if(constraint_max_field):
        constraints['maxfield'] = constraint_max_field
    if(constraint_max_fluence):
        constraints['maxfluence'] = constraint_max_fluence
    if(constraint_max_ionization):
        constraints['maxionization'] = constraint_max_ionization
    return constraints


def _parse_oct_target_params(parser):
    """
    Parses `Target`-section of the input file.

    Parameters
    ----------
    parser : configparser.ConfigParser

    Returns
    -------
    config : dict
        Dictionary of the parameters
    """
    params = parser.items("Optimization")
    for key,_ in params:
        if key not in [
                       'target',
                       'energyrange',
                       'scheme',
                       'penalty_coeff',
                       'max_iterations',
                       'states',
                       'spa_optimizer',
                       'verbose',
                       'bayes_num_cores',
                       'bayes_batch_size'
                      ]:
            raise Exception("No such option: Optimization::" + key)
    
    config = {}
    config['target'] = parser["Optimization"].get("target", "rydberg")
    assert config['target'] in ["rydberg", "pes"]

    config['verbose'] = str_to_bool(
        parser["Optimization"].get("verbose", "False"))

    states_str = parser.get("Optimization", "states").strip()
    config['states'] = _parse_target_states(states_str)
    
    config["optimization_scheme"] = parser.get(
        "Optimization", "scheme").lower().strip()

    if config["optimization_scheme"] not in [
        'isres',
        'orig. direct',
        "orig. direct-l",
        "mlsl+cobyla",
        "mlsl+bobyqa",
        "cobyla",
        "bobyqa",
        "esch",
        "mlsl+praxis",
        "praxis",
        "mma",
        "slsqp",
        "ls-bfgs",
        "pt-newton",
        "slvmv",
        "mlsl+mma",
        "mlsl+slsqp",
        "mlsl+ls-bfgs",
        "mlsl+pt-newton",
        "mlsl+slvmv",
        "bayesian",
            "spa"]:
        raise Exception("Invalid optimization scheme: " +
                        config["optimization_scheme"])

    config["spa_optimizer"] = parser["Optimization"].get(
        "spa_optimizer", "bobyqa").lower().strip()

    config["max_iterations"] = parser["Optimization"].getint(
        "max_iterations", -1)

    config["bayes_num_cores"] = parser["Optimization"].getint(
        "bayes_num_cores", 1)

    config["bayes_batch_size"] = parser["Optimization"].getint(
        "bayes_batch_size", 1)

    return config


def _parse_parameter(string):
    """Parses input string either to a float or list of colon-separated floats"""
    try:
        return float(string)
    except BaseException:
        prange = string.split('#')[0]
        prange = prange.split(':')
        prange = [float(d) for d in prange]
        return prange
    else:
        raise Exception("Invalid option value: " + string)

# Parses a laser output file written by the optimization code
#
# Input:
#    filename     path to the laser output file which we parse
#                 to construct a corresponding laser
# Output:
#    laser        instance of the Laser class


def parse_laserfile(filename):
    laserfile = open(filename)
    line = laserfile.readline().strip()
    line = laserfile.readline().strip()
    laser = Laser()
    while line != "#":
            m = re.match(
                r"#    Pulse: A=(?P<A>[^,]+), w=(?P<w>[^,]+), k=(?P<k>[^,]+), τ=(?P<tau>[^,]+), φ=(?P<cep>[^,]+), σ=(?P<fwhm>[^,]+)",
                line)
            A = Parameter(float(m.group("A")), True)
            w = Parameter(float(m.group("w")), True)
            k = Parameter(float(m.group("k")), True)
            tau = Parameter(float(m.group("tau")), True)
            phi = Parameter(float(m.group("cep")), True)
            sigma = Parameter(float(m.group("fwhm")), True)
            laser.add_pulse(SingleColorLaserPulse(A, w, k, tau, phi, sigma))
            line = laserfile.readline().strip()
    return laser


# Parses a '[PulseN]' section of the input file
#
# Input:
#   parser    an instance of the parser
#   pulsename name of the section to be parsed
# Output:
#   config    dictionary of the laser parameters/variables
#
def _parse_pulse(parser, pulsename):
    params = parser.items(pulsename)
    for key in params:
        if key[0] not in ['a', 'w', 'k', 'tcenter', 'cep', 'fwhm']:
            raise Exception("No such option: " + pulsename + "::" + key[0])

    config = {}
    config['A'] = _parse_parameter(parser.get(pulsename, "A"))
    config['w'] = _parse_parameter(parser.get(pulsename, "w"))
    config['k'] = _parse_parameter(parser.get(pulsename, "k"))
    config['tau'] = _parse_parameter(parser.get(pulsename, "tcenter"))
    config['phi'] = _parse_parameter(parser.get(pulsename, "cep"))
    config['sigma'] = _parse_parameter(parser.get(pulsename, "fwhm"))
    return config

# Parses the input file
#
# Input:
#   path   folder from which we look for the file 'input'
# Output:
#   simulation_config   dictionary of parameters for the simulation of TDSE
#   laser               an instance of the laser class
#   oct_config          dictionary of parameters for the optimization routines
#   variable_limits     upper and lower bound for optimization parameters
#   initial_values      a quess for the initial values
# false/true          true if we should enforce constraint of the zero
# electric field


def get_config(path="./"):
    parser = configparser.ConfigParser()
    out = parser.read(path + "/input")
    if len(out) == 0:
        raise RuntimeError("Input file does not exist: "+
                           path+"/input")

    constraints = {}
    oct_config = None

    # Parse the relevant sections in the input file
    simulation_config = _parse_simulation_params(parser)
    if(parser.has_section("Constraints")):
        constraints = _parse_constraint_params(parser)
    if(parser.has_section("Optimization")):
        oct_config = _parse_oct_target_params(parser)

    # Get names of all sections which define a single-color pulse
    pulsenames = filter(lambda item: item[0:5] == "Pulse", parser.sections())
    variable_limits = np.empty([0, 2])
    initial_values = np.array([])
    # If the file has a section 'laser', then we use only that one,
    # not the pulse sections
    if parser.has_section("Laser"):
        laser = parse_laserfile(path + "/" + parser.get("Laser", "filepath"))
    else:
        laser = Laser()
        random.seed()
        max_field_integral = 0
        # Parse each laser pulse and add it to the total laser
        for pulsename in pulsenames:
            pparams = _parse_pulse(parser, pulsename)
            if(isinstance(pparams['A'], float)):
                A = Parameter(pparams['A'], True)
            else:
                A = Parameter(random.uniform(
                    pparams['A'][0], pparams['A'][1]), False)
                initial_values = np.append(initial_values, A.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['A']]))
            if(isinstance(pparams['w'], float)):
                w = Parameter(pparams['w'], True)
                wmin = w.val
            else:
                w = Parameter(random.uniform(
                    pparams['w'][0], pparams['w'][1]), False)
                wmin = pparams['w'][0]
                initial_values = np.append(initial_values, w.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['w']]))
            if(isinstance(pparams['k'], float)):
                k = Parameter(pparams['k'], True)
            else:
                k = Parameter(random.uniform(
                    pparams['k'][0], pparams['k'][1]), False)
                initial_values = np.append(initial_values, k.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['k']]))
            if(isinstance(pparams['tau'], float)):
                tau = Parameter(pparams['tau'], True)
            else:
                tau = Parameter(random.uniform(
                    pparams['tau'][0], pparams['tau'][1]), False)
                initial_values = np.append(initial_values, tau.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['tau']]))
            if(isinstance(pparams['phi'], float)):
                phi = Parameter(pparams['phi'], True)
            else:
                phi = Parameter(random.uniform(
                    pparams['phi'][0], pparams['phi'][1]), False)
                initial_values = np.append(initial_values, phi.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['phi']]))
            if(isinstance(pparams['sigma'], float)):
                sigma = Parameter(pparams['sigma'], True)
                sigmamin = sigma.val
            else:
                sigma = Parameter(random.uniform(
                    pparams['sigma'][0], pparams['sigma'][1]), False)
                sigmamin = pparams['sigma'][0]
                initial_values = np.append(initial_values, sigma.val)
                variable_limits = np.concatenate(
                    (variable_limits, [pparams['sigma']]))
            laser.add_pulse(SingleColorLaserPulse(A, w, k, tau, phi, sigma))

    simulation_config['calculate-costate'] = False

    return simulation_config, laser, oct_config, constraints, variable_limits, initial_values
