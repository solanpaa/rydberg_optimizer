#!/usr/bin/env python3
'''
common_setup.py

This module contains methods for
(1) setting up the optimization routine
(2) obtaining an initial quess satisfying the pulse constraints

'''
from rydberg_optimizer import *
from rydberg_optimizer.common.laser import *
from rydberg_optimizer.common.constraints import *
from rydberg_optimizer.common.nlopt_bayesian import *
from rydberg_optimizer.common.nlopt_spa import *
import random
import nlopt
import logging
import numpy as np

def setup_constraints(constraints, laser, variable_limits):
    '''
    Sets up and returns constraints.
    Parameters:
        constraints     dict
            List of constraints parsed from the input
        laser           Laser
            an instance of the laser class which has been set up
            with correct number of constant parameters and variable parameters
        variable_limits np.ndarray, shape (N,2)
            limits for the N variable parameters of the laser. First column is
            the lower limit and second the upper limit
    Returns:
        constraint_functions 
            functions that define the constraints of the laser pulse
    '''

    # Get the default logger
    logger = logging.getLogger('')

    # Output constraint information
    if "maxduration" in constraints:
        logger.info("Constraint: Maximum duration (field fwhm): " +
                    constraints["maxduration"])
    if "maxfield" in constraints:
        logger.info("Constraint: Maximum field: " + constraints["maxfield"])
    if "maxfluence" in constraints:
        logger.info("Constraint: Maximum fluence: " +
                    constraints["maxfluence"])
    if "maxionization" in constraints:
        logger.info("Constraint: Maximum ionization: " +
                    constraints["maxionization"])

    constraint_functions = add_constraints(None, constraints, laser)
    return constraint_functions



def setup_constraints_and_optimizers(tconfig, constraints, laser, variable_limits, iteration_result_container):
    '''
    Sets up and returns the optimization routines and constraints.
    Parameters:
        tconfig         dict
            Optimization-section of the input parsed into dictionary.
        constraints     dict
            List of constraints parsed from the input
        laser           Laser
            an instance of the laser class which has been set up
            with correct number of constant parameters and variable parameters
        variable_limits np.ndarray, shape (N,2)
            limits for the N variable parameters of the laser. First column is
            the lower limit and second the upper limit
        iteration_result_container 
            a "reference" to the container which saves
            data from each optimization iteration
            to be passed to the constraint functions
    Returns:
        opt 
            the optimization routinee
        constraint_functions 
            functions that define the constraints of the laser pulse
    '''

    # Get the default logger
    logger = logging.getLogger('')

    # Variables where we assign the optimization routines
    ALopt = None   # Augmented Lagrangian optimizer
    Gopt = None    # Global optimizer
    Lopt = None    # Local optimizer

    # Output constraint information
    if "maxduration" in constraints:
        logger.info("Constraint: Maximum duration (field fwhm): " +
                    constraints["maxduration"])
    if "maxfield" in constraints:
        logger.info("Constraint: Maximum field: " + constraints["maxfield"])
    if "maxfluence" in constraints:
        logger.info("Constraint: Maximum fluence: " +
                    constraints["maxfluence"])
    if "maxionization" in constraints:
        logger.info("Constraint: Maximum ionization: " +
                    constraints["maxionization"])

    constraint_functions = {}

    logger.info("")

    if tconfig["optimization_scheme"] == "spa":
        _, constraint_functions = get_optimizer(
            "spa", constraints, laser, laser.number_of_variables, iteration_result_container, 1, 1)

        def opt_generator(problem_dimensions):
            opt, _ = get_optimizer(tconfig["spa_optimizer"], constraints, laser, problem_dimensions,
                                   iteration_result_container, tconfig["bayes_num_cores"], tconfig["bayes_batch_size"])
            return opt
        maxiter = tconfig["max_iterations"]
        if maxiter == -1:
            maxiter = None
        opt = nlopt_spa(laser.number_of_variables, max_eval=maxiter)
        opt.set_local_optimizer(opt_generator)
    else:
        opt, constraint_functions = get_optimizer(tconfig["optimization_scheme"], constraints, laser, laser.number_of_variables,
                                                  iteration_result_container,  tconfig["bayes_num_cores"], tconfig["bayes_batch_size"])

    # Set the variable limits for the optimizer
    opt.set_lower_bounds(variable_limits[:, 0])
    opt.set_upper_bounds(variable_limits[:, 1])

    if tconfig["max_iterations"] > 0:
        opt.set_maxeval(tconfig["max_iterations"])
        logger.info("Maximum optimization iterations: %d" %
                    tconfig["max_iterations"])

    logger.info("")

    return opt, constraint_functions


def get_optimizer(name, constraints, laser, dim, iteration_result_container,
                  bayes_num_cores, bayes_batch_size):
    logger = logging.getLogger('')
    constraint_functions = {}
    if name == "isres":
        opt = nlopt.opt(nlopt.GN_ISRES, dim)
        opt.set_xtol_rel(0.02)
        opt.set_ftol_rel(0.02)
        constraint_functions = add_constraints(
            opt, constraints, laser, iteration_result_container)
        logger.info("Optimization scheme: ISRES (g+c)")

    elif name == "orig. direct":
        Gopt = nlopt.opt(nlopt.GN_ORIG_DIRECT, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)
        constraint_functions = add_constraints(
            Gopt, constraints, laser, iteration_result_container)
        opt = Gopt
        logger.info("Optimization scheme: Direct (g+c)")

    elif name == "orig. direct-l":
        Gopt = nlopt.opt(nlopt.GN_ORIG_DIRECT_L, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)
        constraint_functions = add_constraints(
            Gopt, constraints, laser, iteration_result_container)
        opt = Gopt
        logger.info("Optimization scheme: Direct-L (g+c)")

    elif name == "esch":
        Gopt = nlopt.opt(nlopt.GN_ESCH, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag (c) + ESCH (g)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: ESCH (g)")

    elif name == "mlsl+cobyla":

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Lopt = nlopt.opt(nlopt.LN_COBYLA, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        add_constraints(Lopt, constraints, laser, iteration_result_container)
        Gopt.set_local_optimizer(Lopt)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + COBYLA (l+c)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + COBYLA (l+c)")

    elif name == "mlsl+bobyqa":

        Lopt = nlopt.opt(nlopt.LN_BOBYQA, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            ALopt_local = nlopt.opt(nlopt.AUGLAG, dim)
            ALopt_local.set_local_optimizer(Lopt)
            add_constraints(ALopt_local, constraints, laser,
                            iteration_result_container)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        if constraints:
            Gopt.set_local_optimizer(ALopt_local)
        else:
            Gopt.set_local_optimizer(Lopt)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + Aug. Lag. (c) + BOBYQA (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme:MLSL (g) + BOBYQA (l)")

    elif name == "mlsl+praxis":
        Lopt = nlopt.opt(nlopt.LN_PRAXIS, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            ALopt_local = nlopt.opt(nlopt.AUGLAG, dim)
            ALopt_local.set_local_optimizer(Lopt)
            add_constraints(ALopt_local, constraints, laser,
                            iteration_result_container)
            Gopt.set_local_optimizer(ALopt_local)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + Aug. Lag. (c) + PRAXIS (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + PRAXIS (l)")

    elif name == "mlsl+mma":
        Lopt = nlopt.opt(nlopt.LD_MMA, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            Gopt.set_local_optimizer(Lopt)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + MMA (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + MMA (l)")

    elif name == "mlsl+slsqp":
        Lopt = nlopt.opt(nlopt.LD_SLSQP, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            Gopt.set_local_optimizer(Lopt)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + SLSQP (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + SLSQP (l)")

    elif name == "mlsl+ls-bfgs":
        Lopt = nlopt.opt(nlopt.LD_LBFGS, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            ALopt_local = nlopt.opt(nlopt.AUGLAG, dim)
            ALopt_local.set_local_optimizer(Lopt)
            add_constraints(ALopt_local, constraints, laser,
                            iteration_result_container)
            Gopt.set_local_optimizer(ALopt_local)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + Aug. Lag. (c) + LS-BFGS (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + LS-BFGS (l)")

    elif name == "mlsl+pt-newton":
        Lopt = nlopt.opt(nlopt.LD_TNEWTON_PRECOND_RESTART, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            ALopt_local = nlopt.opt(nlopt.AUGLAG, dim)
            ALopt_local.set_local_optimizer(Lopt)
            add_constraints(ALopt_local, constraints, laser,
                            iteration_result_container)
            Gopt.set_local_optimizer(ALopt_local)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + Aug. Lag. (c) + PT-Newton (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + PT-Newton (l)")

    elif name == "mlsl+slmvm":
        Lopt = nlopt.opt(nlopt.LD_VAR2, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)

        Gopt = nlopt.opt(nlopt.GN_MLSL, dim)
        Gopt.set_xtol_rel(0.02)
        Gopt.set_ftol_rel(0.02)

        if constraints:
            ALopt_local = nlopt.opt(nlopt.AUGLAG, dim)
            ALopt_local.set_local_optimizer(Lopt)
            add_constraints(ALopt_local, constraints, laser,
                            iteration_result_container)
            Gopt.set_local_optimizer(ALopt_local)
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Gopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info(
                "Optimization scheme: Aug. Lag (c) + MLSL (g) + Aug. Lag. (c) + SLMVM (l)")
        else:
            opt = Gopt
            logger.info("Optimization scheme: MLSL (g) + SLMVM (l)")

    elif name == "cobyla":
        print(dim)
        opt = nlopt.opt(nlopt.LN_COBYLA, dim)
        opt.set_xtol_rel(0.01)
        opt.set_ftol_rel(0.01)
        constraint_functions = add_constraints(
            opt, constraints, laser, iteration_result_container)
        logger.info("Optimization scheme: COBYLA (l+c)")

    elif name == "bobyqa":

        Lopt = nlopt.opt(nlopt.LN_BOBYQA, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Lopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag. (c) + BOBYQA (l)")
        else:
            opt = Lopt
            logger.info("Optimization scheme: BOBYQA (l)")

    elif name == "praxis":
        Lopt = nlopt.opt(nlopt.LN_PRAXIS, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Lopt)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag. (c) + Praxis (l)")
        else:
            opt = Lopt
            logger.info("Optimization scheme: Praxis (l)")

    elif name == "mma":
        opt = nlopt.opt(nlopt.LD_MMA, dim)
        opt.set_xtol_rel(0.01)
        opt.set_ftol_rel(0.01)
        constraint_functions = add_constraints(
            opt, constraints, laser, iteration_result_container)
        logger.info("Optimization scheme: MMA (l+c)")

    elif name == "slsqp":
        opt = nlopt.opt(nlopt.LD_SLSQP, dim)
        opt.set_xtol_rel(0.01)
        opt.set_ftol_rel(0.01)
        constraint_functions = add_constraints(
            opt, constraints, laser, iteration_result_container)
        logger.info("Optimization scheme: SLSQP (l+c)")

    elif name == "ls-bfgs":
        Lopt = nlopt.opt(nlopt.LD_LBFGS, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Lopt)
            opt.set_xtol_rel(0.01)
            opt.set_ftol_rel(0.01)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag. (c) + LS-BFGS (l)")
        else:
            opt = Lopt
            logger.info("Optimization scheme: LS-BFGS (l)")

    elif name == "pt-newton":
        Lopt = nlopt.opt(nlopt.LD_TNEWTON_PRECOND_RESTART, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Lopt)
            opt.set_xtol_rel(0.01)
            opt.set_ftol_rel(0.01)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag. (c) + PT-Newton (l)")
        else:
            opt = Lopt
            logger.info("Optimization scheme: PT-Newton (l)")

    elif name == "slmvm":
        Lopt = nlopt.opt(nlopt.LD_VAR2, dim)
        Lopt.set_xtol_rel(0.01)
        Lopt.set_ftol_rel(0.01)
        if constraints:
            opt = nlopt.opt(nlopt.AUGLAG, dim)
            opt.set_local_optimizer(Lopt)
            opt.set_xtol_rel(0.01)
            opt.set_ftol_rel(0.01)
            constraint_functions = add_constraints(
                opt, constraints, laser, iteration_result_container)
            logger.info("Optimization scheme: Aug. Lag. (c) + SLMVM (l)")
        else:
            opt = Lopt
            logger.info("Optimization scheme: SLMVM (l)")

    elif name == "bayesian":
        if bayes_num_cores > 1:
            opt = nlopt_bayesian(laser.number_of_variables,
                                 parallel=True,
                                 core_num=bayes_num_cores,
                                 batch_size=bayes_batch_size)
        else:
            opt = nlopt_bayesian(laser.number_of_variables)

        logger.info("Optimization scheme: Bayesian (g)")
        opt.set_xtol_abs(1e-3)
    elif name == "spa":
        opt = None
        constraint_functions = add_constraints(
            None, constraints, laser, iteration_result_container)
    else:
        logger.error("Invalid optimization scheme. Programming error.")
        exit(1)

    return opt, constraint_functions



def add_constraints(OPT, constraints, laser, iteration_result_container=None):
    '''
    Constrains the optimization routine
    Input:
      OPT            the optimization routine of Nlopt
      constraints    dictionary of constraints to be used
      laser          the laser routine
      iteration_result_container
                     a "reference" to the container which saves
                     data from each optimization iteration
                     to be passed to the constraint functions
    Output:
       constraint_functions - dictionary of functions that represent the constraints
                              in the optimization parameter space
    '''

    logger = logging.getLogger('')
    # Check each constraint and add the corresponding function to the list
    constraint_functions = {}
    if "maxduration" in constraints:
        def fc_maxdur(x, grad): return max_time_constraint(
            x, grad, laser, float(constraints["maxduration"]))
        constraint_functions["maxduration"] = fc_maxdur
    if "maxfield" in constraints:
        def fc_maxfield(x, grad): return max_field_constraint(
            x, grad, laser, float(constraints["maxfield"]))
        constraint_functions["maxfield"] = fc_maxfield
    if "maxfluence" in constraints:
        def fc_maxfluence(x, grad): return max_fluence_constraint(
            x, grad, laser, float(constraints["maxfluence"]))
        constraint_functions["maxfluence"] = fc_maxfluence
    if "maxionization" in constraints:
        def fc_maxionization(x, grad): return max_ionization_constraint(
            x, grad, laser, iteration_result_container, float(constraints["maxionization"]))

    if OPT:
        # Add each constraint that is used to the optimization routine
        if "maxduration" in constraints:
            OPT.add_inequality_constraint(fc_maxdur, 1e-4)
        if "maxfield" in constraints:
            OPT.add_inequality_constraint(fc_maxfield, 1e-4)
        if "maxfluence" in constraints:
            OPT.add_inequality_constraint(fc_maxfluence, 1e-4)

    return constraint_functions


def get_allowed_initial_quess(laser, variable_limits, constraint_functions):
    '''
    Obtain random values for the laser parameters such that they obey the constraints
    Input:
        laser                  instance of the laser class
        variable_limits        bounds for the variables
        constraint_functions   functions representing the constraints
        initial_quess          some initial quess for the laser parameters
    Output:
        initial_quess          an array containing one possible set of allowed values for the parameters
    '''
    initial_quess = np.zeros(laser.number_of_variables)
    for i in range(laser.number_of_variables):
        initial_quess[i] = random.uniform(
            variable_limits[i, 0], variable_limits[i, 1])
    while True:
        is_ok = True
        # Check all constraints
        for key, fc in constraint_functions.items():
            
            if is_ok:
                if key == 'maxionization':
                    pass
                else:
                    is_ok = (fc(initial_quess, []) <= 0)

        # If we found a good set of parameters, let's finish the routine
        if is_ok:
            break
        else:
            # If not, then let's get a new quess for the parameters
            for i in range(laser.number_of_variables):
                initial_quess[i] = random.uniform(
                    variable_limits[i, 0], variable_limits[i, 1])

    return initial_quess
