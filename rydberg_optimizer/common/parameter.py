#!/usr/bin/env python3

# Class representing a variable or a constant


class Parameter:
    def __init__(self, x, is_const):
        self.val = x
        self.is_const = is_const
