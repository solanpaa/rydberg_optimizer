# Wrapper for GPyOpt with interface
# compatible with nlopt's opt-class
import numpy as np
import GPyOpt


class nlopt_bayesian:

    # Initialization of the optimizer
    def __init__(self, dim, **kwargs):
        self.params = {
            'dim': dim,
            'max_time': None,
            'max_eval': None,
            'xtol_abs': 1e-3,
            'min_objective': None,
            'model': None,
            'lower_bounds': None,
            'upper_bounds': None,
            'last_result': None,
            'last_value': None,
            'BO': None,
            'maximize': False,

        }
        for key, val in kwargs:
            if key in self.params:
                self.params[key] = val
            else:
                raise KeyError(
                    "Provided argument not applicable to the bayesian optimizer routine.")

    # Runs the optimization routine
    # Takes one argument which is the initial quess
    # (a numpy array)
    def optimize(self, *args):
        space = []
        for i in range(self.params['dim']):
            space.append({'name': 'var_{}'.format(i + 1), 'type': 'continuous', 'domain': (
                self.params['lower_bounds'][i], self.params['upper_bounds'][i])})

        if len(args) != 0:
            initial_design_nd = np.atleast_2d(args[0])
        else:
            initial_design_nd = None

        self.params['bo'] = GPyOpt.methods.BayesianOptimization(
            f=self.params['min_objective'],
            domain=space,
            model_type='GP',
            acquisition_type='EI',
            normalize_Y=True,
            exact_feval=True,
            maximize=self.params['maximize'])
        self.params['bo'].run_optimization(
            max_iter=self.params['max_eval'], max_time=self.params['max_time'], eps=self.params['xtol_abs'], verbosity=True)
        x_eval, y_eval = self.params['bo'].get_evaluations()
        self.params['last_value'] = min(y_eval)
        self.params['last_result'] = x_eval[np.argmin(y_eval), :]
        return self.params['last_result']

    # Returns the argument that gives the optimal value
    def last_optimize_result(self):
        return self.params['last_result']

    # Returns the optimal value
    def last_optimum_value(self):
        return self.params['last_value']

    # Returns the algorithm 'name'.
    def get_algorithm(self):
        return "gpyopt_bayesian"

    # Returns the algorithm name
    def get_algorithm_name(self):
        return "GPyOpt bayesian optimizer"

    # Returns the dimension of the search space
    def get_dimension(self):
        return self.params['dim']

    # Set objective for minimization
    def set_min_objective(self, *args):
        def wrapper(x, grad=np.array([])):
            if len(x.shape) != 1:
                return np.array([args[0](elem, grad) for elem in x])
            else:
                return args[0](x, grad)
        self.params['min_objective'] = wrapper

    # Set objective for maximization
    def set_max_objective(self, *args):
        self.set_min_objective((lambda x, grad: args[0](x, grad)))
        self.params['maximize'] = True

    # Adds a scalar-valued inequality constraint: c(x)<= 0
    # Argument 1: the function c
    def add_inequality_constraint(self, *args):
        pass

    # Adds a scalar-valued equality constraint: c(x) == 0
    # Argument 1: the function c
    def add_equality_constraint(self, *args):
        pass

    # Gets lower bounds of the optimization hypercube
    def get_lower_bounds(self, *args):
        return self.params['lower_bounds']

    # Sets lower bounds of the optimization hypercube
    # Argument: float or numpy array
    # If float, all optimization parameters have same lower bound
    # If numpy array, one lower bound per optimization parameter
    def set_lower_bounds(self, *args):
        self.params['lower_bounds'] = args[0]

    # Gets upper bounds of the optimization hypercube
    def get_upper_bounds(self, *args):
        return self.params['upper_bounds']

    # Sets upper bounds of the optimization hypercube
    # Argument: float or numpy array
    # If float, all optimization parameters have same upper bound
    # If numpy array, one upper bound per optimization parameter
    def set_upper_bounds(self, *args):
        self.params['upper_bounds'] = args[0]

    # Gets the stopping criteria for absolute change in the argument value
    def get_xtol_abs(self, *args):
        return self.params['xtol_abs']

    # Sets the stopping criteria for absolute change in the argument value
    # Argument: float or numpy array
    # If float, all optimization parameters must obey same tolerance
    # If numpy array, one tolerance per optimization parameter
    def set_xtol_abs(self, *args):
        self.params['xtol_abs'] = args[0]

    # Gets the maximum number of function evaluations
    def get_maxeval(self):
        return self.params['max_eval']

    # Sets the maximum number of function evaluations
    # Argument: integer
    def set_maxeval(self, *args):
        self.params['max_eval'] = args[0]

    # Gets the maximum simulation time in seconds
    def get_maxtime(self):
        return self.params['max_time']

    # Sets the maximum simulation time in seconds
    # Argument: float
    def set_maxtime(self, *args):
        self.params['max_time'] = args[0]

    # The following are implemented only for the sake of
    # full interface compatibility with nlopt
    def set_initial_step(self, *args):
        raise NotImplementedError

    def set_default_initial_step(self, *args):
        raise NotImplementedError

    def get_initial_step(self, *args):
        raise NotImplementedError

    def get_initial_step_(self, *args):
        raise NotImplementedError

    def force_stop(self):
        raise NotImplementedError

    def get_force_stop(self):
        raise NotImplementedError

    def set_force_stop(self, *args):
        raise NotImplementedError

    def set_local_optimizer(self, *args):
        raise NotImplementedError

    def get_population(self):
        raise NotImplementedError

    def set_population(self, *args):
        raise NotImplementedError

    def get_vector_storage(self):
        raise NotImplementedError

    def set_vector_storage(self, *args):
        raise NotImplementedError

    def add_inequality_mconstraint(self, *args):
        raise NotImplementedError

    def add_equality_mconstraint(self, *args):
        raise NotImplementedError

    def get_stopval(self):
        raise NotImplementedError

    def set_stopval(self, *args):
        raise NotImplementedError

    def get_ftol_rel(self):
        raise NotImplementedError

    def set_ftol_rel(self, *args):
        raise NotImplementedError

    def get_ftol_abs(self):
        raise NotImplementedError

    def set_ftol_abs(self, *args):
        raise NotImplementedError

    def get_xtol_rel(self):
        raise NotImplementedError

    def set_xtol_rel(self, *args):
        raise NotImplementedError

    def remove_inequality_constraints(self):
        raise NotImplementedError

    def remove_equality_constraints(self):
        raise NotImplementedError


if __name__ == '__main__':
    print("Testing")

    def fun(x, grad): return np.sin(x**2)
    opt = nlopt_bayesian(1)
    opt.set_lower_bounds([-1])
    opt.set_upper_bounds([1])
    opt.set_min_objective(fun)
    opt.set_maxeval(20)
    opt.optimize(np.array([[0.5]]))
    opt.params['bo'].plot_acquisition()
    from matplotlib.pyplot import *
    show()
