QPROP-OPT
=========

This Python package contains a modified version of the QPROP code written by
Dieter Bauer et al. [1]. On top of QPROP we've built an optimization
framework that combines simple gaussien enveloped pulses with single carrier 
wavelength pulses to produce an experimentally realizable -- yet flexible multicolor waveform.

The SAE model potentials are taken from MODEL POTENTIALS FOR ALKALI METAL ATOMS
AND Li-LIKE IONS, W. Schweizer, P. Fassbinder, R. Gonzalez-Ferez.

Installation
------------

In order to compile the package, you need an C++17 compliant C++-compiler and
the following libraries:

* `libboost_python3`
* `libboost_timer`
* `openblas`
* `gsl`

In addition, **you need to manually install `nlopt` with python3
support**. Rest of the requirements can be found in requirements.txt 
(you might need to install GPy first and then GPyOpt separately).

Make sure that all the python libraries are found in PYTHONPATH and the C++
libraries by the compiler/linker.

Run::

    $ python3 setup.py build

to compile the C++ package (needs C++17 compliant compiler) and::


    $ python3 setup.py install


to install the whole package.

To run the tests for the python suite, execute::

    $ python3 setup.py test


Package tools overview
----------------------

The optimization suite installs multiple executables to your system.

The executables for optimization simulations are

* ``optimize``             --  runs optimization simulations
* ``analyze_optimization`` --  visualizes the optimization simulation

``optimize`` runs a single optimization simulation, targetting either PES or
occupation of Rydberg states, based on the parameters in your ``input``-file.
The result of each optimization iteration is saved to ``opt_out``-directory

* ``hydrogen_re-wf.dat_<index>``  -- final wavefunction
* ``input``                       -- ``input``-file used by ``optimize``
* ``iter_out_<index>``            -- some basic data of the optimization iteration
* ``laser_<index>``               -- laser electric field
* ``vecpot_<index>``              -- vector potential of the laser pulse

``analyze_optimization`` visualizes these optimization iterations. The topmost
figure shows how well the laser electric field of each optimization iteration
obeys the constraints. The middle figure shows the target value and the amount
of ionization (i.e., how much electron density was absorbed by the boundaries).
The lowest figures show you more details on individual simulations including,
e.g., the laser electric field in time and the final state projected to select
eigenstates of the field-free Hamiltonian.

For running and analyzing a single quantum simulation, we provide the
executables

* ``single_simulation``        -- runs a single quantum simulation
* ``animate_density``          -- animates the electron density on x-z plane
* ``final_state_populations``  -- a single bar-graph of the final populations of field-free states
* ``populations_graph``        -- a graph of populations of select field-free states in time

Note on input file
------------------

Note that the python wrapper is very quickly parsed together. For this reason
you need to many parameters in the input file that do not affect your
simulation. Sorry about that. Contributions are welcomed.

Running single simulations
--------------------------

Create an input file with name ``input`` (use the one at the git repo's root as a starting point)
in the directory where you want your simulations saved. The parameters
affecting the numerical solution of TDSE are located under ``[Simulation]`` section 
of the input file, and they are

    | ``log_to_file`` :: yes/no, whether to save output to a logfile with the executable's name 
    | ``delta-r`` :: float, spacing of the radial grid
    | ``radial-grid-length`` :: float, maximum size of the radial grid without absorbing potential
    | ``ell-grid-size`` :: int, number of angular momentum states to use in the simulation
    | ``imag-width`` :: float, width of the complex absorbing potential
    | ``delta-t`` :: float, time-step
    | ``save-interval`` :: float, how often to save the wavefunction. Use -1 to save only the final state
    | ``atom_type`` :: str, atom type to use. Currently 'H' and 'Li' are implemented.

In addition, you most likely want to set the section ``[Optimization]`` and there
``staterange``-parameter. It's ``:``-separated pair of values, and the upper value
will be used as maximum n-quantum number when analyzing the overlap of the
time-evolving state with the field-free energy-eigenstates.

The laser pulse can be set either as in the optimization section OR you can set
the section ``[Laser]`` with the parameter ``filename``. This parameter should be
the relative path to an laser_XXXXXX-file from the output of optimization
simulation.

Running optimization simulations
--------------------------------

The optimization simulation is defined by the ``[Optimization]`` section
of the ``input``-file. Possible arguments are:

    | ``target`` :: str, rydberg (or pes, hasn't been tested very carefully)
    | ``states`` :: the states we are trying to optimize. 
    | ``scheme`` :: str, see the next section
    | ``spa_optimizer`` :: str, see the next section


In addition, the optimization simulations support constraints defined 
in the ``[Constraints]`` -section of the input. Possible values are

    | ``maxduration``    :: float, maximum fwhm-duration of the laser electric field
    | ``maxfield``       :: float, maximum electric field amplitude
    | ``maxfluence``     :: float, maximum integrated electric field squared
    | ``maxionization``  :: float, maximum ionization of the atom

If you don't want to set any constraints, leave the ``[Constraints]``-section
empty. In case your optimization simulation doesn't start, please check your
constraints. They may be impossible with the pulse configuration you've set up.

Defining multicomponent laser-pulses
------------------------------------

The total laser pulse is a sum over component pulses
with gaussian envelope and a single-frequency carrier wave.

Each component pulse can be defined in a section called ``[PulseN]`` where N is
integer. Each parameter can be either a fixed value or if we wish to optimize
it, we can set two colon-separated values. Possible parameters are

    | ``A`` :: float, field amplitude
    | ``w``       :: float, carrier frequency
    | ``k``       :: float, carrier chirp
    | ``tcenter`` :: float, maximum of the pulse envelope in time
    | ``cep``     :: float, carrier envelope phase
    | ``fwhm``    :: float, fwhm of the electric _field_ envelope

Available optimization schemes
------------------------------

The optimization method is set with the option `scheme`` under ``[Optimization]``
section of the input file. The possible values are

  * isres
  * orig. direct
  * orig. direct-l
  * esch
  * mlsl+cobyla
  * mlsl+bobyqa
  * mlsl+praxis
  * cobyla
  * bobyqa
  * praxis
  * bayesian
  * spa
    - spa requires also option ``spa_optimizer`` which can be any of the above
      (except ``spa``)