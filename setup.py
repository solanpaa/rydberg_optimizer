#!/usr/bin/env python3
from setuptools import setup, find_packages
from setuptools.extension import Extension
import setuptools.command.build_py
import rydberg_optimizer as my_pkg

qprop_basefiles = [
    'bar.cc',
    'ylm.cc',
    'wavefunction.cc',
    'hamop.cc',
    'grid.cc',
    'fluid.cc',
    'factorial.cc',
    'winop.cc',
    'parameter.cc',
    'cmatrix.cc',
    'ned.cc',
    'small_helpers.cc',
    'interp_function.cc']
qprop_sources = ['real_time_propagation.cc', 'eval-tsurff.cc', 'core.cc']
qprop_filepaths = ["rydberg_optimizer/qprop/core/base/%s" %
                   s for s in qprop_basefiles] + ["rydberg_optimizer/qprop/core/%s" %
                                                  s for s in qprop_sources]


setup(
    name='rydberg_optimizer',
    author=my_pkg.__author__,
    author_email=my_pkg.__author_email__,
    classifiers=[
        'License :: OSI Approved :: Gnu General Public License version 3',
        'Programming Language :: Python :: 3.6',
    ],
    description='Optimization of laser-driven processes in atoms',
    long_description=open("README.rst","r").read(),
    entry_points = {
        'console_scripts' : [
                'optimize = rydberg_optimizer.optimize:optimize_main',
                'single_simulation = rydberg_optimizer.single_simulation:single_main',
                'analyze_optimization = rydberg_optimizer.analysis.analyze:run_optimization_analysis',
                'animate_density = rydberg_optimizer.analysis.animate_density:run_animate',
                'final_state_populations = rydberg_optimizer.analysis.plot_final_state_overlaps:plot_overlaps',
                'populations_graph = rydberg_optimizer.analysis.populations_graph:plot_populations',
                'simulation_overview = rydberg_optimizer.analysis.simulation_overview:simulation_overview',
                'precalculate_states = rydberg_optimizer.precalculate_rydberg_states:precalculate_states_main'
        ]
    },
    ext_modules=[
        Extension(
                'rydberg_optimizer.qprop.core',
                qprop_filepaths,
                library_dirs=[
                    '/usr/lib/x86_64-linux-gnu/'
                ],
                include_dirs=[
                    'rydberg_optimizer/qprop/core/',
                    'rydberg_optimizer/qprop/core/base/'],
                extra_compile_args=[
                    '-std=c++17',
                    '-fPIC',
                    '-DHAVE_BOOST',
                ],
                libraries=[
                    'python3.6m',
                    'boost_python-py36',
                    'gsl',
                    'gslcblas',
                    'openblas']),
    ],
    install_requires=['numpy', 'scipy', 'matplotlib', 'h5py','GPyOpt', 'GPy'],
    keywords='physics',
    license=my_pkg.__license__,
    packages=find_packages(),
    test_suite='nose2.collector.collector',
    tests_require=['nose2'],
    url='https://gitlab.com/qcad.fi/rydberg_optimizer',
    version=my_pkg.__version__,
    zip_safe=True)
